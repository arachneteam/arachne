#include <arachne/poset/poset_util.hpp>

#include <queue>

namespace arn {

namespace impl {

std::vector<Poset::size_type> getInDegrees(const Poset& poset) {
    std::vector<Poset::size_type> inDegrees(poset.size(), 0);
    for (Poset::element_type v = 0; v < poset.size(); v++) {
        for (Poset::element_type u : poset.getOutgoingEdges(v)) {
            inDegrees[u]++;
        }
    }
    return inDegrees;
}

std::queue<Poset::element_type> getMinElements(const std::vector<Poset::size_type>& inDegrees,
                                        const Poset& poset) {
    std::queue<Poset::element_type> minElements;
    for (Poset::element_type v = 0; v < poset.size(); v++) {
        if (inDegrees[v] == 0) {
            minElements.push(v);
        }
    }
    return minElements;
}

void stripElement(unsigned int v, std::queue<Poset::element_type>& minElements,
                  std::vector<Poset::size_type>& inDegrees, const Poset& poset) {
    for (Poset::element_type u : poset.getOutgoingEdges(v)) {
        if (--inDegrees[u] == 0) {
            minElements.push(u);
        }
    }
}

} // namespace impl

std::vector<std::pair<Poset::element_type, Poset::element_type>> minPairDecomposition(const Poset& poset) {
    std::vector<std::pair<Poset::element_type, Poset::element_type>> decomposition;
    std::vector<Poset::size_type> inDegrees = impl::getInDegrees(poset);
    std::queue<Poset::element_type> minElements = impl::getMinElements(inDegrees, poset);
    while (!minElements.empty()) {
        Poset::element_type v1 = minElements.front();
        minElements.pop();
        Poset::element_type v2 = v1;
        if (!minElements.empty()) {
            v2 = minElements.front();
            minElements.pop();
        }
        decomposition.push_back(std::make_pair(v1, v2));
        impl::stripElement(v1, minElements, inDegrees, poset);
        if (v1 != v2) {
            impl::stripElement(v2, minElements, inDegrees, poset);
        }
    }
    return decomposition;
}

} // namespace arn
