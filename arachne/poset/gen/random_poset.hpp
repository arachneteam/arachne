#ifndef ARACHNE_RANDOM_POSET_HPP
#define ARACHNE_RANDOM_POSET_HPP

#include <arachne/poset/poset.hpp>

namespace arn {

//! Generator of random posets
/**
 * A simple pseudo-random poset generator.
 *
 * Generated posets are \b not equally probable and satisfy the following invariant:
 * if x is less than y according to the poset relation, then x is numerically less than y.
 *
 * A poset is generated from a directed acyclic graph (DAG) built by a generator by
 * randomly deciding for every pair of elements whether an edge between them should exist.
 */
class RandomPosetGenerator {
public:
    /**
     * Creates a random poset generator with a specified configuration consisting of
     * maximum size of generated posets and probability of edge occurrence in the backing DAG.
     * @param maxSize - maximum size of a generated poset
     * @param edgeProbability - probability of edge existence in the backing DAG
     */
    RandomPosetGenerator(Poset::size_type maxSize = 100, double edgeProbability = 0.25);

    /**
     * Generates a random poset of a random size, not greater than the maximum size.
     * @return Random poset of a random size
     */
    Poset generate();

    /**
     * Generates a random poset of a given size, which must not be greater than the maximum size.
     * @param size - poset size
     * @return Random poset of a given size
     */
    Poset generate(Poset::size_type size);

private:
    Poset::size_type maxSize;
    double edgeProbability;
};

} // namespace arn

#endif
