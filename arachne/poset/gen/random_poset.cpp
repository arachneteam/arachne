#include <arachne/poset/gen/random_poset.hpp>
#include <arachne/poset/poset_builder.hpp>

#include <cstdlib>
#include <ctime>
#include <stdexcept>

namespace arn {

RandomPosetGenerator::RandomPosetGenerator(Poset::size_type maxSize, double edgeProbability)
        : maxSize(maxSize), edgeProbability(edgeProbability) {
    std::srand(static_cast<unsigned int>(std::time(0)));
}

Poset RandomPosetGenerator::generate() {
    return generate(rand() % maxSize);
}

Poset RandomPosetGenerator::generate(Poset::size_type size) {
    if (size > maxSize) {
        throw std::range_error("Requested size is bigger than maximum size.");
    }
    PosetBuilder posetBuilder(size);
    for (Poset::element_type i = 0; i < size; i++) {
        for (Poset::element_type j = i + 1; j < size; j++) {
            if (rand() % 100 < edgeProbability * 100) {
                posetBuilder.add(i, j);
            }
        }
    }
    return posetBuilder.build();
}

} // namespace arn
