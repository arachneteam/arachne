#ifndef ARACHNE_LINEXT_HPP
#define ARACHNE_LINEXT_HPP

#include <arachne/poset/poset.hpp>
#include <arachne/poset/linear_extension.hpp>

#include <boost/coroutine2/coroutine.hpp>

#include <functional>
#include <vector>

namespace arn {

typedef boost::coroutines2::coroutine<LinearExtension>::pull_type LinearExtensionIterator;
typedef boost::coroutines2::coroutine<LinearExtension>::push_type LinearExtensionConsumer;

//! Generator of linear extensions of a poset
/**
 * Generator of all linear extensions of an arbitrary poset.
 *
 * For every linear extensions executes callbacks registered with forEachExtension() method.
 *
 * This implementation uses the algorithm described in
 * <em>Gara Pruesse, Frank Ruskey. Generating Linear Extensions Fast.
 * SIAM J. Comput. Vol. 23, No. 2 (1994), pp. 373-386.</em>
 */
class LinearExtensionGenerator {
public:
    typedef std::function<void(LinearExtension)> Callback;

    //! Aggregator of statistics for the set of linear extensions of a poset
    /**
     * Objects of this class store various statistics gathered during the process of
     * generating all linear extensions of a poset by a linear extension generator.
     */
    class Statistics {
    public:
        /**
         * Returns total number of linear extensions.
         * @return Total number of linear extensions
         */
        int countTotal() const;

        /**
         * Returns number of linear extensions which contain relation less < greater.
         * @param less - the less of the two elements
         * @param greater - the greater of the two elements
         * @return Number of linear extensions which contain relation less < greater
         */
        int countWithOrder(Poset::element_type less, Poset::element_type greater) const;

        /**
         * Returns probability that a random linear extension contains relation less < greater.
         * @param less - the less of the two elements
         * @param greater - the greater of the two elements
         * @return Probability that a random linear extension contains relation less < greater
         */
        double probabilityOfOrder(Poset::element_type less, Poset::element_type greater) const;

        /**
         * Returns average height of a given poset element across all linear extensions.
         * The height of a minimum element is 1 and the height of a maximum element
         * is equal to the poset size.
         * @param x - poset element
         * @return Average height of a poset element across all linear extensions
         */
        double averageHeight(Poset::element_type x) const;
    private:
        Statistics();
        void init(const std::vector<Poset::element_type>& li);

        int extensionCounter;
        std::vector<std::vector<int>> xyRunSum;
        std::vector<std::vector<int>> xyRunStart;
        std::vector<int> xRunSum;
        std::vector<int> xRunStart;
        friend class LinearExtensionGenerator;
    };

    /**
     * Registers a callback function that will be executed for every generated linear extension.
     * @param callback - function to call for every generated linear extension
     * @return The same generator this method was called on
     */
    LinearExtensionGenerator& forEachExtension(Callback callback);

    /**
     * Generates all linear extensions of a given poset and for each one executes
     * all registered callbacks. The order in which linear extensions are generated
     * is not specified and may differ between subsequent invocations. Time complexity
     * of the generation procedure is \a O(e(P)) where \a e(P) is the number of linear
     * extensions of a poset \a P i.e. every extension is generated in constant amortized time.
     * @param poset - poset for which linear extensions shall be generated
     * @return Statistics describing the set of all linear extensions of a given poset
     */
    Statistics generate(const Poset& poset);

    /**
     * Removes all callbacks registered for this generator.
     */
    void reset();

private:
    bool plus;
    std::vector<Poset::element_type> le;
    std::vector<Poset::size_type> li;
    std::vector<Poset::element_type> leftMin, rightMin;
    std::vector<Callback> callbacks;
    Statistics statistics;

    int init(const Poset& poset);
    void subGen(int idx, const Poset& poset);
    void exchange(int idx);
    bool canShiftRight(Poset::element_type x, const Poset& poset, int stopper = -1);
    void shiftRight(Poset::element_type x);
    void shiftLeft(Poset::element_type x);
    void eval();
    void updateStatisticsSignChange();
    void updateStatisticsSwap(Poset::element_type less, Poset::element_type greater);
    void updateStatisticsFinish();
};

/**
 * Returns an input iterator over the set of all linear extensions of a given poset.
 * Linear extensions are generated lazily i.e. a new linear extension is generated
 * in constant amortized time on demand, only when the iterator is incremented.
 * @param poset - poset for which linear extensions shall be generated
 * @return Input iterator over the set of all linear extensions of a given poset
 */
LinearExtensionIterator linearExtensions(const Poset& poset);

} // namespace arn

#endif
