#include <arachne/poset/gen/linext.hpp>
#include <arachne/poset/poset_util.hpp>

namespace arn {

LinearExtensionGenerator& LinearExtensionGenerator::forEachExtension(Callback callback) {
    callbacks.push_back(callback);
    return *this;
}

LinearExtensionGenerator::Statistics LinearExtensionGenerator::generate(const Poset& poset) {
    int startIndex = init(poset);
    eval();
    subGen(startIndex, poset);
    updateStatisticsFinish();
    return std::move(statistics);
}

void LinearExtensionGenerator::reset() {
    callbacks.clear();
}

int LinearExtensionGenerator::init(const Poset& poset) {
    le.clear();
    li.resize(poset.size() + 1);
    leftMin.clear();
    rightMin.clear();
    auto decomposition = minPairDecomposition(poset);
    for (auto minPair : decomposition) {
        Poset::element_type a = minPair.first;
        Poset::element_type b = minPair.second;
        if (a == b) {
            li[a] = le.size();
            le.push_back(a);
        } else {
            li[a] = le.size();
            le.push_back(a);
            li[b] = le.size();
            le.push_back(b);
            leftMin.push_back(a);
            rightMin.push_back(b);
        }
    }
    int minPairs = leftMin.size();
    leftMin.push_back(poset.size());
    rightMin.push_back(poset.size());
    li[poset.size()] = le.size();
    plus = true;
    statistics.init(li);
    return minPairs;
}

void LinearExtensionGenerator::subGen(int idx, const Poset& poset) {
    if (idx < 0) {
        return;
    }
    subGen(idx - 1, poset);
    int rightMinShifts = 0;
    bool bothShiftable = false;
    while (canShiftRight(rightMin[idx], poset)) {
        ++rightMinShifts;
        shiftRight(rightMin[idx]);
        subGen(idx - 1, poset);
        int leftMinShifts = 0;
        while (canShiftRight(leftMin[idx], poset, rightMin[idx])) {
            bothShiftable = true;
            ++leftMinShifts;
            shiftRight(leftMin[idx]);
            subGen(idx - 1, poset);
        }
        if (bothShiftable) {
            exchange(idx - 1);
            subGen(idx - 1, poset);
            int returningShifts = rightMinShifts & 1 ? leftMinShifts - 1 : leftMinShifts + 1;
            while (returningShifts--) {
                shiftLeft(leftMin[idx]);
                subGen(idx - 1, poset);
            }
        }
    }
    if (bothShiftable && (rightMinShifts & 1)) {
        shiftLeft(leftMin[idx]);
    } else {
        exchange(idx - 1);
    }
    subGen(idx - 1, poset);
    while (rightMinShifts--) {
        shiftLeft(rightMin[idx]);
        subGen(idx - 1, poset);
    }
}

inline void LinearExtensionGenerator::exchange(int idx) {
    if (idx < 0) {
        updateStatisticsSignChange();
        plus = !plus;
    } else {
        updateStatisticsSwap(leftMin[idx], rightMin[idx]);
        std::swap(le[li[leftMin[idx]]], le[li[rightMin[idx]]]);
        std::swap(li[leftMin[idx]], li[rightMin[idx]]);
        std::swap(leftMin[idx], rightMin[idx]);
    }
    eval();
}

bool LinearExtensionGenerator::canShiftRight(Poset::element_type x, const Poset& poset, int stopper) {
    return li[x] + 1 < le.size() && le[li[x] + 1] != stopper && (poset[x] || poset[le[li[x] + 1]]);
}

void LinearExtensionGenerator::shiftRight(Poset::element_type x) {
    Poset::element_type y = le[li[x] + 1];
    updateStatisticsSwap(x, y);
    std::swap(le[li[x]], le[li[x] + 1]);
    std::swap(li[x], li[y]);
    eval();
}

void LinearExtensionGenerator::shiftLeft(Poset::element_type x) {
    Poset::element_type y = le[li[x] - 1];
    updateStatisticsSwap(y, x);
    std::swap(le[li[x]], le[li[x] - 1]);
    std::swap(li[x], li[y]);
    eval();
}

void LinearExtensionGenerator::eval() {
    if (!plus) {
        return;
    }
    for (Callback callback : callbacks) {
        callback(LinearExtension(le, li));
    }
}

void LinearExtensionGenerator::updateStatisticsSignChange() {
    statistics.extensionCounter++;
}

void LinearExtensionGenerator::updateStatisticsSwap(Poset::element_type less, Poset::element_type greater) {
    statistics.xyRunSum[less][greater] += statistics.extensionCounter - statistics.xyRunStart[less][greater];
    statistics.xyRunStart[less][greater] = -1;
    statistics.xyRunStart[greater][less] = statistics.extensionCounter;
    statistics.xRunSum[less] += (li[less] + 1) * (statistics.extensionCounter - statistics.xRunStart[less]);
    statistics.xRunStart[less] = statistics.extensionCounter;
    statistics.xRunSum[greater] += (li[greater] + 1) * (statistics.extensionCounter - statistics.xRunStart[greater]);
    statistics.xRunStart[greater] = statistics.extensionCounter;
    statistics.extensionCounter++;
}

void LinearExtensionGenerator::updateStatisticsFinish() {
    for (Poset::element_type x = 0; x < li.size(); x++) {
        statistics.xRunSum[x] += (li[x] + 1) * (statistics.extensionCounter - statistics.xRunStart[x]);
        for (Poset::element_type y = 0; y < li.size(); y++) {
            if (statistics.xyRunStart[x][y] >= 0) {
                statistics.xyRunSum[x][y] += statistics.extensionCounter - statistics.xyRunStart[x][y];
            }
        }
    }
}

LinearExtensionGenerator::Statistics::Statistics() {
}

void LinearExtensionGenerator::Statistics::init(const std::vector<Poset::size_type>& li) {
    extensionCounter = 1;
    xyRunSum.resize(li.size(), std::vector<int>(li.size(), 0));
    xyRunStart.resize(li.size(), std::vector<int>(li.size()));
    for (Poset::element_type x = 0; x < li.size(); x++) {
        for (Poset::element_type y = 0; y < li.size(); y++) {
            xyRunStart[x][y] = li[x] < li[y] ? 0 : -1;
        }
    }
    xRunSum.resize(li.size(), 0);
    xRunStart.resize(li.size(), 0);
}

int LinearExtensionGenerator::Statistics::countTotal() const {
    return extensionCounter / 2;
}

int LinearExtensionGenerator::Statistics::countWithOrder(Poset::element_type less, Poset::element_type greater) const {
    return xyRunSum[less][greater] / 2;
}

double LinearExtensionGenerator::Statistics::probabilityOfOrder(Poset::element_type less, Poset::element_type greater) const {
    return static_cast<double>(xyRunSum[less][greater]) / extensionCounter;
}

double LinearExtensionGenerator::Statistics::averageHeight(Poset::element_type x) const {
    return static_cast<double>(xRunSum[x]) / extensionCounter;
}

LinearExtensionIterator linearExtensions(const Poset& poset) {
    return LinearExtensionIterator([&](LinearExtensionConsumer& yield) {
        LinearExtensionGenerator()
                .forEachExtension([&](arn::LinearExtension le) {
                    yield(le);
                })
                .generate(poset);
    });
}

} // namespace arn
