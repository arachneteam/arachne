#ifndef ARACHNE_POSET_HPP
#define ARACHNE_POSET_HPP

#include <vector>

//! Namespace of Arachne library
namespace arn {

//! Model class for a poset
/**
 * This class represents a finite partially ordered set (poset in short).
 * Elements of an n-element poset are indexed by consecutive integers from 0 to n - 1.
 *
 * A poset instance is backed by a directed acyclic graph (DAG) which is sometimes
 * referred to as a Hasse diagram. In addition, a transitive closure of the DAG is maintained
 * in order to be able to compare any two elements in constant time.
 *
 * Two static methods are provided for creating two trivial posets:
 * - total order in which every two elements are comparable
 * - total unorder in which every two elements are incomparable
 *
 * See also PosetBuilder for convenient interface for poset creation.
 */
class Poset {
public:
    using size_type = std::size_t;
    using element_type = std::size_t;

    //! Model class for a poset element
    /**
     * A wrapper class for poset elements providing operators to compare elements
     * according to the poset relation. Instances of this class are returned by
     * Poset::operator[]().
     */
    class Element {
    public:
        /**
         * Compares two poset elements and returns true iff the elements are comparable and
         * the first one is less than the second according to the poset relation.
         * @param el - poset element which this element is compared with
         * @return True iff this element is less than the other element
         * according to the poset relation
         */
        bool operator<(const Element& el) const;

        /**
         * Compares two poset elements and returns true iff the elements are comparable and
         * the first one is less than or equal to the second according to the poset relation.
         * @param el - poset element which this element is compared with
         * @return True iff this element is less than or equal to the other element
         * according to the poset relation
         */
        bool operator<=(const Element& el) const;

        /**
         * Compares two poset elements and returns true iff the elements are comparable and
         * the first one is greater than the second according to the poset relation.
         * @param el - poset element which this element is compared with
         * @return True iff this element is greater than the other element
         * according to the poset relation
         */
        bool operator>(const Element& el) const;

        /**
         * Compares two poset elements and returns true iff the elements are comparable and
         * the first one is greater than or equal to the second according to the poset relation.
         * @param el - poset element which this element is compared with
         * @return True iff this element is greater than or equal to the other element
         * according to the poset relation
         */
        bool operator>=(const Element& el) const;

        /**
         * Compares two poset elements and returns true iff the elements are incomparable
         * according to the poset relation.
         * @param el - poset element which this element is compared with
         * @return True iff this element is incomparable with the other element
         * according to the poset relation
         */
        bool operator||(const Element& el) const;

        /**
         * Compares two poset elements and returns true iff they are the same element.
         * @param el - poset element which this element is compared with
         * @return True iff this element is equal to the other element
         */
        bool operator==(const Element& el) const;

        /**
         * Compares two poset elements and returns true iff they are not the same element.
         * @param el - poset element which this element is compared with
         * @return True iff this element is not equal to the other element
         */
        bool operator!=(const Element& el) const;

    private:
        const Poset& poset;
        size_type idx;

        Element(const Poset& poset, size_type idx);
        friend class Poset;
    };

    /**
     * Constructs a poset of a given size with relation between elements
     * defined by a given DAG (transitivity of the input DAG is not required).
     * @param size - poset size
     * @param edges - DAG representing relation between poset elements
     */
    Poset(size_type size, const std::vector<std::vector<element_type>>& edges);

    /**
     * Returns size of this poset.
     * @return Poset size
     */
    Poset::size_type size() const;

    /**
     * Returns an element descriptor which can be used
     * for comparisons according to the relation of this poset.
     * @param el - poset element
     * @return Element descriptor
     */
    Poset::Element operator[](element_type el) const;

    /**
     * Returns a vector of outgoing edges from a poset element,
     * not including implicit transitive relations.
     * @param el - poset element
     * @return Vector of outgoing edges
     */
    const std::vector<element_type>& getOutgoingEdges(element_type el) const;

    /**
     * Creates a poset being a total order i.e.
     * a poset where every two elements are comparable
     * according to the natural ordering of their integer values.
     * @param size - size of total order
     * @return Total order of a given size
     */
    static Poset totalOrder(Poset::size_type size);

    /**
     * Creates a poset being a total unorder i.e.
     * a poset where every two elements are incomparable.
     * @param size - size of total unorder
     * @return Total unorder of a given size
     */
    static Poset totalUnorder(Poset::size_type size);

private:
    size_type posetSize;
    std::vector<std::vector<element_type>> edges;
    std::vector<std::vector<bool>> transitiveClosure;

    void calculateTransitiveClosure();
};

} // namespace arn

#endif
