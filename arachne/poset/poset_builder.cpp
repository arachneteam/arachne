#include <arachne/poset/poset_builder.hpp>

namespace arn {

PosetBuilder::PosetBuilder(Poset::size_type posetSize) : posetSize(posetSize) {
    edges.resize(posetSize);
}

PosetBuilder& PosetBuilder::add(Poset::element_type less, Poset::element_type greater) {
    edges[less].push_back(greater);
    return *this;
}

PosetBuilder& PosetBuilder::addChain(std::initializer_list<Poset::element_type> chain) {
    if (chain.size() > 0) {
        for (auto less = chain.begin(), greater = std::next(less); greater != chain.end(); less++, greater++) {
            add(*less, *greater);
        }
    }
    return *this;
}

Poset PosetBuilder::build() const {
    return Poset(posetSize, edges);
}

} // namespace arn
