#ifndef ARACHNE_LINEAR_EXTENSION_HPP
#define ARACHNE_LINEAR_EXTENSION_HPP

#include <arachne/poset/poset.hpp>

#include <vector>

namespace arn {

//! Lightweight wrapper for a linear extension of a poset
/**
 * A lightweight wrapper class for representing a linear extension of a poset.
 * An instance of this class holds a reference to a permutation vector defining
 * a linear extension and a reference to its inverse to be able to quickly
 * locate particular element in the permutation. E.g. for a permutation vector
 * [1, 2, 3, 0] the corresponding inverse is [3, 0, 1, 2].
 */
class LinearExtension {
public:
    /**
     * Constructs a linear extension defined by a permutation vector together with its inverse.
     * The object is backed by the provided vectors so any later modification
     * of the vectors will influence this instance.
     * @param le - permutation vector
     * @param li - inverse permutation vector
     */
    LinearExtension(const std::vector<Poset::element_type>& le, const std::vector<Poset::size_type>& li);

    /**
     * Returns the poset element at a given position in this linear extension.
     * @param idx - position in this linear extension
     * @return Poset element at a given position in this linear extension
     */
    Poset::element_type operator[](Poset::size_type idx) const;

    /**
     * Returns the position of a given poset element in this linear extension.
     * @param el - poset element
     * @return position of a given poset element in this linear extension
     */
    Poset::size_type idx(Poset::element_type el) const;

    /**
     * Returns the size of this linear extension.
     * @return size of this linear extension
     */
    Poset::size_type size() const;

private:
    const std::vector<Poset::element_type>& le;
    const std::vector<Poset::size_type>& li;
};

} // namespace arn

#endif
