#ifndef ARACHNE_POSET_UTIL_HPP
#define ARACHNE_POSET_UTIL_HPP

#include <arachne/poset/poset.hpp>

namespace arn {

/**
 * Successively strips off pairs of minimal elements from a poset and aggregates them in a vector.
 * If there is a unique minimum element x at some point of the process, it is stripped off
 * singly and a pair (x,x) is added to the decomposition.
 * @param poset - poset to be decomposed
 * @return Minimal pair decomposition of a given poset
 */
std::vector<std::pair<Poset::element_type, Poset::element_type>> minPairDecomposition(const Poset& poset);

} // namespace arn

#endif
