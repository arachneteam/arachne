#include <arachne/poset/linear_extension.hpp>

namespace arn {

LinearExtension::LinearExtension(const std::vector<Poset::element_type>& le,
                                 const std::vector<Poset::size_type>& li)
        : le(le), li(li) {
}

Poset::element_type LinearExtension::operator[](Poset::size_type idx) const {
    return le[idx];
}

Poset::size_type LinearExtension::idx(Poset::element_type el) const {
    return li[el];
}

Poset::size_type LinearExtension::size() const {
    return le.size();
}

} // namespace arn
