#include <arachne/poset/poset.hpp>

#include <queue>

namespace arn {

Poset::Poset(size_type size, const std::vector<std::vector<element_type>>& edges)
        : posetSize(size), edges(edges) {
    calculateTransitiveClosure();
}

Poset Poset::totalOrder(Poset::size_type size) {
    std::vector<std::vector<Poset::element_type>> edges(size);
    for (Poset::element_type x = 0; x + 1 < size; x++) {
        edges[x].push_back(x + 1);
    }
    return Poset(size, edges);
}

Poset Poset::totalUnorder(Poset::size_type size) {
    return Poset(size, std::vector<std::vector<Poset::element_type>>(size));
}

Poset::size_type Poset::size() const {
    return posetSize;
}

const std::vector<Poset::element_type>& Poset::getOutgoingEdges(element_type el) const {
    return edges[el];
}

Poset::Element Poset::operator[](element_type el) const {
    return Element(*this, el);
}

Poset::Element::Element(const Poset& poset, size_type idx) : poset(poset), idx(idx) {
}

bool Poset::Element::operator<(const Element& el) const {
    return operator!=(el) && operator<=(el);
}

bool Poset::Element::operator<=(const Element& el) const {
    return poset.transitiveClosure[idx][el.idx];
}

bool Poset::Element::operator>(const Element& el) const {
    return operator!=(el) && operator>=(el);
}

bool Poset::Element::operator>=(const Element& el) const {
    return poset.transitiveClosure[el.idx][idx];
}

bool Poset::Element::operator||(const Element& el) const {
    return !operator<=(el) && !operator>(el);
}

bool Poset::Element::operator==(const Element& el) const {
    return idx == el.idx;
}

bool Poset::Element::operator!=(const Element &el) const {
    return !operator==(el);
}

void Poset::calculateTransitiveClosure() {
    transitiveClosure.resize(posetSize, std::vector<bool>(posetSize, false));
    std::queue<Poset::element_type> bfsQueue;
    for (Poset::element_type x = 0; x < posetSize; x++) {
        transitiveClosure[x][x] = true;
        bfsQueue.push(x);
        while (!bfsQueue.empty()) {
            Poset::element_type y = bfsQueue.front();
            bfsQueue.pop();
            for (Poset::element_type z : edges[y]) {
                if (!transitiveClosure[x][z]) {
                    transitiveClosure[x][z] = true;
                    bfsQueue.push(z);
                }
            }
        }
    }
}

} // namespace arn
