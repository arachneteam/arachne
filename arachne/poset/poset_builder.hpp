#ifndef ARACHNE_POSET_BUILDER_HPP
#define ARACHNE_POSET_BUILDER_HPP

#include <arachne/poset/poset.hpp>

#include <initializer_list>

namespace arn {

//! Builder for constructing posets
/**
 * This is a helper class for creating posets in a handy way. Any transitive relations
 * are assembled by the builder and do not need to be provided explicitly.
 *
 * Instances of this class can be reused and modified between subsequent calls of
 * build() method with no impact on already created posets.
 *
 * The following code creates a four-element "diamond poset":
 * \code{.cpp}
 * arn::Poset p = arn::PosetBuilder(4)
 *         .add(0, 1).add(1, 3)
 *         .add(0, 2).add(2, 3)
 *         .build();
 * \endcode
 * and it is equivalent to the code below:
 * \code{.cpp}
 * arn::Poset p = arn::PosetBuilder(4)
 *         .addChain({0, 1, 3})
 *         .addChain({0, 2, 3})
 *         .build();
 * \endcode
 */
class PosetBuilder {
public:
    /**
     * Constructs a poset builder with initial specification for a poset
     * of a given size where every two elements are incomparable (total unorder).
     * @param posetSize - poset size
     */
    PosetBuilder(Poset::size_type posetSize);

    /**
     * Adds a relation between two poset elements to the poset specification: less < greater.
     * @param less - the less of the two elements
     * @param greater - the greater of the two elements
     * @return The same poset builder this method was called on
     */
    PosetBuilder& add(Poset::element_type less, Poset::element_type greater);

    /**
     * Adds a given chain to the poset specification. This is equivalent to calling
     * add() method for every two adjacent elements of the chain.
     * @param chain - chain of elements from the minimal to the maximal
     * @return The same poset builder this method was called on
     */
    PosetBuilder& addChain(std::initializer_list<Poset::element_type> chain);

    /**
     * Creates a poset according to the provided specification.
     * Later modifications of the specification do not affect the created poset.
     * @return Poset fulfilling provided specification
     */
    Poset build() const;

private:
    Poset::size_type posetSize;
    std::vector<std::vector<Poset::element_type>> edges;
};

} // namespace arn

#endif
