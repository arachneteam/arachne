//
// Created by Łukasz Majcher on 04.09.2017.
//

#include "cycle_remover.hpp"
#include <arachne/layouter/utils/graph_utils.hpp>

namespace arn {

GraphLayout& CycleRemover::execute(GraphLayout& graph) {

    for (auto edge : greedyFas(graph)) {
        edge->getHead()->removeNodeFromOutEdges(edge->getTail());
        edge->getTail()->removeNodeFromInEdges(edge->getHead());

        edge->getHead()->addInEdge(edge);
        edge->getTail()->addOutEdge(edge);

        Node* tmp = edge->getHead();
        edge->setHead(edge->getTail());
        edge->setTail(tmp);
        edge->setInverted(true);
    }

    // fix node ids
    graph = GraphUtils::setNodeIds(graph);
    graph = GraphUtils::setNodeInAndOutEdges(graph);
    return graph;
}

std::vector<Edge*> CycleRemover::greedyFas(GraphLayout& graph) {

    std::vector<Edge*> result;

    GraphLayout copyGraph;
    for (auto node : graph.getNodes()) {
        copyGraph.addNode(node);
    }

    Node* sink;
    Node* source;
    Node* maxNode;
    while (copyGraph.size() > 0) {
        while ((sink = getSink(copyGraph)) != nullptr) {
            removeNode(copyGraph, sink);
        }
        while ((source = getSource(copyGraph)) != nullptr) {
            removeNode(copyGraph, source);
        }
        maxNode = getMaxNode(copyGraph);
        const std::vector<Edge*>& edges = removeNode(copyGraph, maxNode);
        result.insert(result.end(), edges.begin(), edges.end());
    }

    return result;
}

Node* CycleRemover::getSink(GraphLayout& graph) {
    for (auto node : graph.getNodes()) {
        if (node->getOutEdges().empty()) {
            return node;
        }
    }
    return nullptr;
}

/*
 * node id -> position node in nodes array
 */
const std::vector<Edge*> CycleRemover::removeNode(GraphLayout& graph, Node* node) {
    if (node == nullptr) {
        return std::vector<Edge*>();
    }

    int id = node->getId();
    graph.nodes.back()->setId(id);
    std::swap(graph.nodes[id], graph.nodes.back());
    graph.nodes.pop_back();
    graph.nameToNodeMap.erase(node->getName());

    for (auto edge : node->getInEdges()) {
        edge->getHead()->removeNodeFromOutEdges(node);
    }

    for (auto edge : node->getOutEdges()) {
        edge->getTail()->removeNodeFromInEdges(node);
    }

    return node->getInEdges();
}

Node* CycleRemover::getSource(GraphLayout& graph) {
    for (auto node : graph.getNodes()) {
        if (node->getInEdges().empty()) {
            return node;
        }
    }
    return nullptr;
}

Node* CycleRemover::getMaxNode(GraphLayout& graph) {
    Node* result = nullptr;
    size_t best = 0;
    for (auto node : graph.getNodes()) {
        if (result == nullptr || node->getOutEdges().size() > node->getInEdges().size() + best) {
            result = node;
            best = node->getOutEdges().size() - node->getInEdges().size();
        }
    }
    return result;
}

} // namespace arn