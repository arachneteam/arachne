//
// Created by Łukasz Majcher on 04.09.2017.
//

#ifndef ARACHNE_ACYCLIC_H
#define ARACHNE_ACYCLIC_H

#include <arachne/layouter/model/graph_layout.hpp>

namespace arn {
class CycleRemover {
public:
    static GraphLayout& execute(GraphLayout &graph);

    static std::vector<Edge*> greedyFas(GraphLayout &layout);

    static Node *getSink(GraphLayout &layout);

    static const std::vector<Edge *> removeNode(GraphLayout &layout, Node *pNode);

    static Node *getSource(GraphLayout &layout);

    static Node *getMaxNode(GraphLayout &layout);
};
} // namespace arn

#endif // ARACHNE_ACYCLIC_H
