//
// Created by Łukasz Majcher on 05.09.2017.
//

#ifndef ARACHNE_NETWORK_SIMPLEX_H
#define ARACHNE_NETWORK_SIMPLEX_H

#include <arachne/layouter/model/graph_layout.hpp>

namespace arn {
class NetworkSimplex {

public:
    static void rank(arn::GraphLayout& layout);
};
} // namespace arn

#endif // ARACHNE_NETWORK_SIMPLEX_H
