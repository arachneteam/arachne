//
// Created by Łukasz Majcher on 05.09.2017.
//

#include "feasible_tree.hpp"

namespace arn {
arn::Tree FeasibleTree::findFeasibleTree(arn::GraphLayout& graph) {
    Tree tree(graph.size());
    // add first node from graph to tree
    tree.setRoot(tree.addNode(0));

    Edge* edge;
    int delta;
    int size = graph.size();
    while (findTightTree(tree, graph) < size) {
        edge = findMinEdge(tree, graph);
        if (tree.containsNode(edge->getHead()->getId())) {
            delta = slack(edge);
        } else {
            delta = -slack(edge);
        }
        updateRanks(tree, graph, delta);
    }

    return tree;
}

int FeasibleTree::findTightTree(Tree& tree, GraphLayout& graph) {
    for (auto node : tree.getNodes()) {
        dfs(node->getId(), tree, graph);
    }
    return tree.size();
}

int FeasibleTree::slack(Edge* edge) {
    return edge->getTail()->getRank() - edge->getHead()->getRank() - edge->getMinLen();
}

Edge* FeasibleTree::findMinEdge(Tree& tree, GraphLayout& graph) {

    Edge* bestEdge = nullptr;
    int bestSlack = std::numeric_limits<int>::max();

    for (auto edge : graph.getEdges()) {
        if (tree.containsNode(edge->getHead()->getId()) !=
            tree.containsNode(edge->getTail()->getId())) {
            if (slack(edge) < bestSlack) {
                bestEdge = edge;
                bestSlack = slack(edge);
            }
        }
    }
    return bestEdge;
}

void FeasibleTree::dfs(int nodeId, Tree& tree, GraphLayout& graph) {
    Node* node = graph.getNodeById(nodeId);
    for (auto edge : node->getInEdges()) {
        Node* head = edge->getHead();
        if (!tree.containsNode(head->getId()) && slack(edge) == 0) {
            tree.addNode(head->getId());
            tree.addEdge(node->getId(), head->getId(), edge->getWeight(), edge->getMinLen());
            dfs(head->getId(), tree, graph);
        }
    }

    for (auto edge : node->getOutEdges()) {
        Node* tail = edge->getTail();
        if (!tree.containsNode(tail->getId()) && slack(edge) == 0) {
            tree.addNode(tail->getId());
            tree.addEdge(node->getId(), tail->getId(), edge->getWeight(), edge->getMinLen());
            dfs(tail->getId(), tree, graph);
        }
    }
}

void FeasibleTree::updateRanks(Tree& tree, GraphLayout& graph, int delta) {
    for (auto treeNode : tree.getNodes()) {
        Node* node = graph.getNodeById(treeNode->getId());
        node->setRank(node->getRank() + delta);
    }
}

} // namespace arn