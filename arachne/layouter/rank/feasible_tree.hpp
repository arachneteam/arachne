//
// Created by Łukasz Majcher on 05.09.2017.
//

#ifndef ARACHNE_FEASIBLE_TREE_H
#define ARACHNE_FEASIBLE_TREE_H

#include <arachne/layouter/model/graph_layout.hpp>
#include <arachne/layouter/model/tree.hpp>

namespace arn {
class FeasibleTree {
public:
    static arn::Tree findFeasibleTree(arn::GraphLayout& layout);

    static int findTightTree(Tree& layout, GraphLayout& graphLayout);

    static void dfs(int id, Tree& tree, GraphLayout& graph);

    static int slack(Edge* pEdge);

    static Edge* findMinEdge(Tree& tree, GraphLayout& layout);

    static void updateRanks(Tree& tree, GraphLayout& layout, int delta);
};
} // namespace arn

#endif // ARACHNE_FEASIBLE_TREE_H
