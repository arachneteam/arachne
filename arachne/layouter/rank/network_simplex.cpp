//
// Created by Łukasz Majcher on 05.09.2017.
//

#include "network_simplex.hpp"
#include "feasible_tree.hpp"
#include <arachne/layouter/utils/graph_utils.hpp>
#include <arachne/layouter/utils/tree_utils.hpp>

namespace arn {
void NetworkSimplex::rank(arn::GraphLayout& graph) {
    graph = GraphUtils::assignRank(graph);
    Tree tree = FeasibleTree::findFeasibleTree(graph);
    TreeUtils::assignPostorder(tree.getRoot(), graph.size());
    TreeUtils::assignCutValues(tree, graph);

    Edge* e;
    Edge* f;
    while ((e = TreeUtils::leaveEdge(tree, graph)) != nullptr) {
        f = TreeUtils::enterEdge(tree, graph, e);
        TreeUtils::exchange(tree, graph, e, f);
    }

    TreeUtils::normalizeRanks(graph);
    TreeUtils::removeEmptyRanks(graph);
    TreeUtils::normalizeRanks(graph);
}

} // namespace arn