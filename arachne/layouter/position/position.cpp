//
// Created by Łukasz Majcher on 08.09.2017.
//

#include "position.hpp"
#include <algorithm>

namespace arn {

int Position::X_ALIGN = 50;
int Position::X_SEPARATION = 50;
int Position::Y_ALIGN = 50;
int Position::Y_SEPARATION = 50;
int Position::MAX_ITERATION = 14;

void Position::position(std::vector<std::vector<Node*>> layers) {
    positionX(layers);
    positionY(layers);
}

void Position::positionX(std::vector<std::vector<Node*>> layers) {
    int prevX = X_ALIGN;

    for (int id = 0; id < layers.size(); id++) {
        int maxWidth = 0;
        for (auto node : layers[id]) {
            maxWidth = std::max(maxWidth, node->getWidth());
        }
        for (auto node : layers[id]) {
            node->setX(prevX + maxWidth / 2);
        }
        prevX += X_SEPARATION + maxWidth;
    }
}

void Position::positionY(std::vector<std::vector<Node*>> layers) {

    initPositionY(layers);

    for (int i = 0; i < MAX_ITERATION; i++) {
        medianPosition(layers, i);
        removeAlign(layers);
    }

    setFinalYPosition(layers);
}

void Position::medianPosition(std::vector<std::vector<Node*>>& order, int iter) {
    if (iter % 2 == 0) {
        for (int r = 1; r < order.size(); r++) {
            std::vector<double> median;
            int lastPosition = Y_ALIGN;
            for (int v = 0; v < order[r].size(); v++) {
                lastPosition = positionValue(order[r][v], true, lastPosition);
            }
        }
    } else {
        for (int r = order.size() - 2; r >= 0; r--) {
            std::vector<double> median;
            int lastPosition = Y_ALIGN;
            for (int v = 0; v < order[r].size(); v++) {
                lastPosition = positionValue(order[r][v], false, lastPosition);
            }
        }
    }
}

int Position::positionValue(Node*& v, bool leftToRight, int lastPosition) {
    int position;
    std::vector<int> p = adjPosition(v, leftToRight);
    int m = (int)(p.size() / 2);
    if (p.empty()) {
        position = std::max(v->getPosition(), (int)(lastPosition + v->getHeight() / 2));
    } else if (p.size() % 2 == 1) {
        position = std::max(p[m], (int)(lastPosition + v->getHeight() / 2));
    } else {
        position = std::max((int)((p[m - 1] + p[m]) / 2.0), (int)(lastPosition + v->getHeight()));
    }

    lastPosition = position + v->getHeight() / 2 + Y_SEPARATION;
    v->setPosition(position);
    return lastPosition;
}

std::vector<int> Position::adjPosition(Node* v, bool leftToRight) {
    std::vector<int> positions;
    if (leftToRight) {
        for (auto edge : v->getInEdges()) {
            Node* x = edge->getHead();
            positions.push_back(x->getPosition());
        }
    } else {
        for (auto edge : v->getOutEdges()) {
            Node* x = edge->getTail();
            positions.push_back(x->getPosition());
        }
    }

    std::sort(positions.begin(), positions.end());
    return positions;
}

void Position::initPositionY(std::vector<std::vector<Node*>>& order) {
    for (int id = 0; id < order.size(); id++) {
        int prevY = Y_ALIGN;
        for (auto node : order[id]) {
            node->setY(prevY + node->getHeight() / 2);
            prevY += Y_SEPARATION + node->getHeight();
            node->setPosition(node->getY());
        }
    }
}

void Position::setFinalYPosition(std::vector<std::vector<Node*>>& order) {
    for (int id = 0; id < order.size(); id++) {
        for (auto node : order[id]) {
            node->setY(node->getPosition());
        }
    }
}

void Position::removeAlign(std::vector<std::vector<Node*>>& order) {
    int align = std::numeric_limits<int>::max();

    for (int r = 0; r < order.size(); r++) {
        align = std::min(align, order[r][0]->getPosition());
    }
    align -= Y_ALIGN;
    for (int r = 0; r < order.size(); r++) {
        for (auto node : order[r]) {
            node->setPosition(node->getPosition() - align);
        }
    }
}

} // namespace arn