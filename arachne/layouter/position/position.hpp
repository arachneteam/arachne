//
// Created by Łukasz Majcher on 08.09.2017.
//

#ifndef ARACHNE_POSITION_H
#define ARACHNE_POSITION_H

#include <arachne/layouter/model/graph_layout.hpp>

namespace arn {
class Position {
public:
    static int X_ALIGN;
    static int X_SEPARATION;
    static int Y_ALIGN;
    static int Y_SEPARATION;

    static int MAX_ITERATION;

    static void position(std::vector<std::vector<Node*>> graph);
    static void positionX(std::vector<std::vector<Node*>> graph);

    static void positionY(std::vector<std::vector<Node*>> vector);

    static void medianPosition(std::vector<std::vector<Node*>>& order, int i);

    static int positionValue(Node *&v, bool leftToRight, int i);

    static std::vector<int> adjPosition(Node *pNode, bool right);

    static void initPositionY(std::vector<std::vector<Node *>> &order);

    static void setFinalYPosition(std::vector<std::vector<Node *>> &order);

    static void removeAlign(std::vector<std::vector<Node *>> &order);
};
} // namespace arn

#endif // ARACHNE_POSITION_H
