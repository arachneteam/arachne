//
// Created by Łukasz Majcher on 30.08.2017.
//

#ifndef ARACHNE_LAYOUTER_H
#define ARACHNE_LAYOUTER_H

#include <arachne/layouter/model/graph_layout.hpp>


namespace arn {
class Layouter {
public:
    virtual GraphLayout& execute(GraphLayout& graph) = 0;
    GraphLayout& preprocess(GraphLayout& graph);

    void setNodeWidthAndHeight(Node *pNode);

    unsigned int getWidth(Node *basic_string);

    unsigned int getHeight(Node *basic_string);
};
} // namespace arn

#endif // ARACHNE_LAYOUTER_H
