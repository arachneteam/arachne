//
// Created by Łukasz Majcher on 30.08.2017.
//

#include "layouter.hpp"
#include <arachne/layouter/utils/string_utils.hpp>

namespace arn {
GraphLayout& Layouter::preprocess(GraphLayout& graph) {
    for (auto node : graph.getNodes()) {
        setNodeWidthAndHeight(node);
    }
    return graph;
}

void Layouter::setNodeWidthAndHeight(Node* node) {
    if(node->getIsDummy()){
        node->setWidth(0);
        node->setHeight(0);
        node->setLabel("");
    }else {
        node->setWidth(getWidth(node));
        node->setHeight(getHeight(node));
    }
}

unsigned int Layouter::getWidth(Node* node) {
    unsigned int width = 0;
    for (auto line : StringUtils::split(node->getLabel(), "\n")) {
        line.length();
        width = std::max(width, static_cast<unsigned int>(line.length()));
    }
    return (unsigned int) (0.5 * width * node->getFontSize() + 18);
}

unsigned int Layouter::getHeight(Node* node) {
    unsigned int height = (unsigned int)StringUtils::split(node->getLabel(), "\n").size();
    return (unsigned int) (height * node->getFontSize() + (height + 1) * (0.3 * node->getFontSize()));
}
} // namespace arn