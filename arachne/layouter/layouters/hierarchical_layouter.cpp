//
// Created by Łukasz Majcher on 04.09.2017.
//

#include "hierarchical_layouter.hpp"
#include <arachne/layouter/order/order.hpp>
#include <arachne/layouter/position/position.hpp>
#include <arachne/layouter/rank/cycle_remover.hpp>
#include <arachne/layouter/rank/network_simplex.hpp>
#include <arachne/layouter/utils/graph_utils.hpp>
#include <cstdlib>

namespace arn {
GraphLayout& HierarchicalLayouter::execute(GraphLayout& graph) {
    graph = GraphUtils::setNodeIds(graph);
    graph = GraphUtils::makeSimpleGraph(graph);
    graph = GraphUtils::setNodeInAndOutEdges(graph);
    graph = CycleRemover::execute(graph);
    NetworkSimplex::rank(graph);
    graph = GraphUtils::normalizeEdges(graph);
    std::vector<std::vector<Node*>> layers = Order::ordering(graph);
    graph = preprocess(graph);
    Position::position(layers);

    srand((unsigned)time(0));

    graph.setHeight(1000);
    graph.setWidth(1000);

    for (auto node : graph.getNodes()) {
        int x = rand() % (graph.getWidth() - node->getWidth()) + node->getWidth() / 2;
        int y = rand() % (graph.getHeight() - node->getHeight()) + node->getHeight() / 2;
        // node->setX(x);
        //node->setY(y);
    }

    return graph;
}

} // namespace arn