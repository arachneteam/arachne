//
// Created by Łukasz Majcher on 04.09.2017.
//

#ifndef ARACHNE_HIERARCHICAL_LAYOUTER_H
#define ARACHNE_HIERARCHICAL_LAYOUTER_H

#include "layouter.hpp"
#include <arachne/layouter/model/graph_layout.hpp>

namespace arn {

class HierarchicalLayouter : public Layouter {
public:
    GraphLayout& execute(GraphLayout& graph);
};
} // namespace arn

#endif // ARACHNE_HIERARCHICAL_LAYOUTER_H
