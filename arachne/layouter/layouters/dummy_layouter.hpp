//
// Created by Łukasz Majcher on 30.08.2017.
//

#ifndef ARACHNE_DUMMY_LAYOUTER_H
#define ARACHNE_DUMMY_LAYOUTER_H

#include "layouter.hpp"
#include <arachne/layouter/model/graph_layout.hpp>

namespace arn {
class DummyLayouter : public Layouter {
public:
    GraphLayout& execute(GraphLayout& graph);
};
} // namespace arn

#endif // ARACHNE_DUMMY_LAYOUTER_H
