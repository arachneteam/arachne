//
// Created by Łukasz Majcher on 30.08.2017.
//

#include <cstdlib>
#include "dummy_layouter.hpp"

namespace arn {
GraphLayout& DummyLayouter::execute(GraphLayout& graph) {
    graph = preprocess(graph);

    srand((unsigned)time(0));


    graph.setHeight(1000);
    graph.setWidth(1000);

    for(auto node: graph.getNodes()){
        int x = rand()% (graph.getWidth()-node->getWidth()) + node->getWidth()/2;
        int y = rand()% (graph.getHeight()-node->getHeight()) + node->getHeight()/2;
        node->setX(x);
        node->setY(y);
    }

    return graph;
}

} // namespace arn
