//
// Created by Łukasz Majcher on 27.08.2017.
//

#include "svg_converter.hpp"
#include <arachne/layouter/utils/string_utils.hpp>
#include <cmath>
#include <fstream>

namespace arn {

const std::string SvgConverter::SVG_HEADER =
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
        "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n"
        " \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">";

void SvgConverter::convert(GraphLayout& graph, std::string fileName) {
    std::fstream file(fileName, std::ios::out);
    if (file.good()) {
        file << SVG_HEADER << "\n\n";
        file << createSvgElement(graph) << "\n\n";
        file.close();
    }
}

void SvgConverter::drawNode(arn::Element* parentElement, Node* node) {
    Element* nodeElement = new Element("g");
    nodeElement->addAttribute("id", node->getName());
    nodeElement->addAttribute("class", "node");
    parentElement->addChild(nodeElement);

    Element* titleElement = new Element("title");
    titleElement->addTextValue(node->getLabel());
    nodeElement->addChild(titleElement);

    Element* ellipseElement = new Element("ellipse");
    ellipseElement->addAttribute("fill", "none");
    ellipseElement->addAttribute("stroke-width", "2");
    ellipseElement->addAttribute("stroke", "black");
    ellipseElement->addAttribute("cx", std::to_string(node->getX()));
    ellipseElement->addAttribute("cy", std::to_string(node->getY()));
    ellipseElement->addAttribute("rx", std::to_string(node->getWidth() / 2));
    ellipseElement->addAttribute("ry", std::to_string(node->getHeight() / 2));
    nodeElement->addChild(ellipseElement);

    createTextElement(nodeElement, node);
}

void SvgConverter::createTextElement(Element* nodeElement, Node* node) {
    Element* textElement = new Element("text");
    textElement->addAttribute("text-anchor", "middle");
    textElement->addAttribute("x", std::to_string(node->getX()));
    textElement->addAttribute("y", std::to_string(node->getY() - node->getHeight() / 2));
    textElement->addAttribute("font-family", "Times,serif");
    textElement->addAttribute("font-size", std::to_string(node->getFontSize()));
    nodeElement->addChild(textElement);

    for (auto line : StringUtils::split(node->getLabel(), "\n")) {
        Element* spanElement = new Element("tspan");
        spanElement->addAttribute("x", std::to_string(node->getX()));
        spanElement->addAttribute("dy", "1.2em");
        spanElement->addTextValue(line);
        textElement->addChild(spanElement);
    }
}

void SvgConverter::drawEdge(Element* parentElement, Edge* edge, GraphLayout& graph) {
    Element* edgeElement = new Element("g");
    edgeElement->addAttribute("id", std::to_string(edge->getId()));
    edgeElement->addAttribute("class", "edge");
    parentElement->addChild(edgeElement);

    Element* titleElement = new Element("title");
    titleElement->addTextValue(edge->getLabel());
    edgeElement->addChild(titleElement);

    drawLine(edgeElement, edge, graph);
}

void SvgConverter::drawLine(Element* parentElement, Edge* edge, GraphLayout& graph) {
    unsigned int begin_x;
    unsigned int begin_y;
    unsigned int end_x;
    unsigned int end_y;
    if (edge->getHead()->getX() < edge->getTail()->getX()) {
        begin_x = edge->getHead()->getX() + edge->getHead()->getWidth() / 2;
        begin_y = edge->getHead()->getY();
        end_x = edge->getTail()->getX() - edge->getTail()->getWidth() / 2;
        end_y = edge->getTail()->getY();
    } else {
        end_x = edge->getTail()->getX() + edge->getTail()->getWidth() / 2;
        end_y = edge->getTail()->getY();
        begin_x = edge->getHead()->getX() - edge->getHead()->getWidth() / 2;
        begin_y = edge->getHead()->getY();
    }

    bool drawArrow = !edge->getTail()->getIsDummy();
    if (edge->isInverted()) {
        std::swap(begin_x, end_x);
        std::swap(begin_y, end_y);
        drawArrow = !edge->getHead()->getIsDummy();
    }

    Element* lineElement = new Element("line");
    lineElement->addAttribute("x1", std::to_string(begin_x));
    lineElement->addAttribute("y1", std::to_string(begin_y));
    lineElement->addAttribute("x2", std::to_string(end_x));
    lineElement->addAttribute("y2", std::to_string(end_y));
    lineElement->addAttribute("stroke", "black");
    lineElement->addAttribute("stroke-width", "2");

    if (graph.isIsDigraph() && drawArrow) {
        lineElement->addAttribute("marker-end", "url(#arrow)");
    }

    parentElement->addChild(lineElement);
}

std::string SvgConverter::createSvgElement(GraphLayout& graph) {
    Element* svgElement = new Element("svg");
    svgElement->addAttribute("width", std::to_string(graph.getWidth()) + "pt");
    svgElement->addAttribute("height", std::to_string(graph.getHeight()) + "pt");
    svgElement->addAttribute("viewBox", "0.00 0.00 " + std::to_string(graph.getWidth()) + " " +
                                                std::to_string(graph.getHeight()));
    svgElement->addAttribute("xmlns", "http://www.w3.org/2000/svg");
    svgElement->addAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");

    createMarkerElement(svgElement);

    Element* graphElement = new Element("g");
    graphElement->addAttribute("id", "graph0");
    graphElement->addAttribute("class", "graph");
    svgElement->addChild(graphElement);

    for (auto node : graph.getNodes()) {
        drawNode(graphElement, node);
    }

    for (auto edge : graph.getEdges()) {
        drawEdge(graphElement, edge, graph);
    }

    return svgElement->print("");
}

void SvgConverter::createMarkerElement(Element* parentElement) {
    Element* defsElement = new Element("defs");
    parentElement->addChild(defsElement);

    Element* markerElement = new Element("marker");
    markerElement->addAttribute("id", "arrow");
    markerElement->addAttribute("markerWidth", "10");
    markerElement->addAttribute("markerHeight", "10");
    markerElement->addAttribute("refX", "9");
    markerElement->addAttribute("refY", "3");
    markerElement->addAttribute("orient", "auto");
    markerElement->addAttribute("markerUnits", "strokeWidth");
    defsElement->addChild(markerElement);

    Element* pathElement = new Element("path");
    pathElement->addAttribute("d", "M0,0 L0,6 L9,3 z");
    pathElement->addAttribute("fill", "#000");
    markerElement->addChild(pathElement);
}

} // namespace arn
