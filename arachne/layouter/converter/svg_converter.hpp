//
// Created by Łukasz Majcher on 27.08.2017.
//

#ifndef ARACHNE_GRAPH_TO_DOT_CONVERTER_H
#define ARACHNE_GRAPH_TO_DOT_CONVERTER_H

#include "converter.hpp"
#include "element.hpp"
#include <arachne/layouter/model/graph_layout.hpp>

namespace arn {
class SvgConverter : public Converter {
public:
    void convert(GraphLayout& graph, std::string file);

    static const std::string SVG_HEADER;

    void drawNode(arn::Element* pElement, Node* pNode);

    void createTextElement(Element* nodeElement, Node* node);

    void drawEdge(Element *pElement, Edge *pEdge, GraphLayout &layout);

    void drawLine(Element *pElement, Edge *pEdge, GraphLayout &layout);

    std::string createSvgElement(GraphLayout& graph);

    void createMarkerElement(Element *pElement);
};
} // namespace arn

#endif // ARACHNE_GRAPH_TO_DOT_CONVERTER_H
