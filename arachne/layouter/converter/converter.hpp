//
// Created by Łukasz Majcher on 27.08.2017.
//

#ifndef ARACHNE_CONVERTER_H
#define ARACHNE_CONVERTER_H

#include <arachne/layouter/model/graph_layout.hpp>

namespace arn {
class Converter {
public:
    virtual void convert(GraphLayout& graph, std::string file) = 0;
};
} // namespace arn

#endif // ARACHNE_CONVERTER_H
