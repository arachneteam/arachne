//
// Created by Łukasz Majcher on 28.08.2017.
//

#include "element.hpp"

namespace arn {
Element::Element(std::string name) {
    this->name = name;
}
void Element::addAttribute(std::string name, std::string value) {
    attributes[name] = value;
}
void Element::addChild(Element* element) {
    children.push_back(element);
}
void Element::addTextValue(std::string value) {
    this->textValue = value;
}

std::string Element::print() {
    return print("");
}
std::string Element::print(std::string prefix) {
    std::string result = prefix + "<";
    result.append(name);

    std::map<std::string, std::string>::iterator it = attributes.begin();
    while (it != attributes.end()) {
        result.append(" " + it->first + "=" + "\"" + it->second + "\"");
        it++;
    }
    result.append(">");
    if (children.empty()) {
        result.append(textValue);
    } else {
        result.append("\n");
        for (auto child : children) {
            result.append(child->print(prefix + "   "));
        }
        result.append(prefix);
    }
    result.append("</" + name + ">");
    result.append("\n");
    return result;
}
} // namespace arn