//
// Created by Łukasz Majcher on 28.08.2017.
//

#ifndef ARACHNE_ELEMENT_H
#define ARACHNE_ELEMENT_H

#include <map>
#include <string>
#include <vector>

namespace arn {
class Element {
public:
    Element(std::string name);
    void addAttribute(std::string name, std::string value);
    void addChild(Element* element);
    void addTextValue(std::string value);
    std::string print(std::string prefix);
    std::string print();


private:
    std::map<std::string, std::string> attributes;
    std::string name;
    std::vector<Element*> children;
    std::string textValue;
};
} // namespace arn

#endif // ARACHNE_ELEMENT_H
