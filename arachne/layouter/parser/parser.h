//
// Created by Łukasz Majcher on 18.08.2017.
//

#ifndef ARACHNE_PARSER_H
#define ARACHNE_PARSER_H

#include "arachne/layouter/model/graph_layout.hpp"
#include <string>

namespace arn {

class Parser {
public:
    GraphLayout& parse(std::string file);

private:
    GraphLayout graphLayout;

    void parseGraphLine(std::string line);
    void trimNode(std::string& node);
    void parseData(std::string data);
    void validateNode(std::string& node);
    bool containsInvalidCharacters(std::string& node);
    bool tryParseAsNode(std::string basic_string, GraphLayout& layout);
    std::string fixNewLineEncoding(std::string basic_string);
};

} // namespace arn

#endif // ARACHNE_PARSER_H
