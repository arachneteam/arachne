#include "parser.h"
#include "parse_exception.h"

#include <arachne/layouter/utils/string_utils.hpp>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>

namespace arn {

GraphLayout& Parser::parse(std::string file) {
    std::fstream graphFile;
    graphFile.open(file, std::ios::in);

    if (graphFile.good()) {

        std::string textLine;
        std::string result;
        int state = 0;
        int state_before_comment = 0;

        while (!graphFile.eof()) {
            getline(graphFile, textLine);

            std::vector<std::string> tokens = StringUtils::split(textLine, " ");
            for (std::string token : tokens) {

                if (state == 1) {
                    if (token == "*/") {
                        state = state_before_comment;
                    }
                    continue;
                }

                if (token == "//") {
                    break;
                } else if (token == "/*") {
                    state_before_comment = state;
                    state = 1;
                } else if (token == "graph") {
                    state = 2;
                } else if (token == "digraph") {
                    graphLayout.setIsDigraph(true);
                    state = 3;
                } else if (token == "{") {
                    state = 4;
                } else if (token == "}") {
                    state = 5;
                } else if (state == 2 || state == 3) {
                    graphLayout.setName(token);
                    state = 6;
                } else if (state == 4) {
                    result.append(token + " ");
                }
            }
            result.append("\n");
        }

        if (state != 5) {
            throw ParseException(std::string("Illegal state the end token not found: }"));
        }

        parseData(result);
        graphFile.close();
    } else {
        throw ParseException("Problem with reading file");
    }

    return graphLayout;
}

void Parser::parseData(std::string result) {
    for (auto line : StringUtils::split(result, "\n")) {
        for (auto chunk : StringUtils::split(line, ";")) {
            parseGraphLine(chunk);
        }
    }
}

void Parser::parseGraphLine(std::string line) {

    if (tryParseAsNode(line, graphLayout) == true) {
        return;
    }

    std::string separator = "--";
    if (graphLayout.isIsDigraph()) {
        separator = "->";
    }

    std::vector<std::string> nodes = StringUtils::split(line, separator);
    for (std::string& node : nodes) {
        trimNode(node);
        if (nodes.size() > 1) {
            validateNode(node);
        }
    }

    for (auto i = 0; i < nodes.size() - 1; i++) {
        graphLayout.addEdge(nodes[i], nodes[i + 1]);

        if (!graphLayout.isIsDigraph()) {
            graphLayout.addEdge(nodes[i + 1], nodes[i]);
        }
    }
}

void Parser::trimNode(std::string& node) {
    node = arn::StringUtils::trim(node);
}

void Parser::validateNode(std::string& node) {
    if (node.length() == 0 || containsInvalidCharacters(node)) {
        throw ParseException(std::string("Invalid node id: " + node));
    }
}

bool Parser::containsInvalidCharacters(std::string& node) {
    return node.find_first_of("!@#$%^&*()+-") != std::string::npos;
}

bool Parser::tryParseAsNode(std::string line, GraphLayout& graph) {
    std::regex rgx("^\\s*([a-zA-Z0-9]+)\\s*(\\[\\s*label\\s*=\\s*\"(.*)\"\\s*\\])?\\s*$");
    std::smatch matches;

    if (std::regex_search(line, matches, rgx)) {
        std::string name = matches[1];
        std::string label = matches[3];

        label = fixNewLineEncoding(label);

        Node* node = graph.getOrAddNode(name);
        if (!label.empty()) {
            node->setLabel(label);
        }
        return true;
    }

    return false;
}

std::string Parser::fixNewLineEncoding(std::string label) {
    return StringUtils::join(StringUtils::split(label, "\\n"), "\n");
}

} // namespace arn