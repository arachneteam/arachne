//
// Created by Łukasz Majcher on 26.08.2017.
//

#ifndef ARACHNE_PARSE_EXCEPTION_H
#define ARACHNE_PARSE_EXCEPTION_H

#include <exception>
#include <string>


namespace arn {
class ParseException : public std::exception {
public:
    ParseException(std::string message);
     const char * what() const throw();
    std::string getMessage() const;
private:
    std::string message;
};
} // namespace arn

#endif // ARACHNE_PARSE_EXCEPTION_H
