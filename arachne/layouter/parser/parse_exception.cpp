//
// Created by Łukasz Majcher on 26.08.2017.
//

#include "parse_exception.h"

namespace arn {
ParseException::ParseException(std::string message) {
    this->message = message;
}

const char * ParseException::what() const throw() {
    return ("Error while parsing dot file. " + message).c_str();
}

    std::string ParseException::getMessage() const{
        return this->message;
    }
} // namespace arn