//
// Created by Łukasz Majcher on 07.08.2017.
//

#include "node.hpp"
#include <vector>

namespace arn {

Node::Node(std::string name) {
    this->name = name;
}

Node::Node(std::string name, std::string label) {
    this->name = name;
    this->label = label;
}

const std::string& Node::getLabel() const {
    return label;
}

const std::string& Node::getName() const {
    return name;
}

void Node::setLabel(const std::string& label) {
    Node::label = label;
}

unsigned int Node::getX() const {
    return x;
}

void Node::setX(unsigned int x) {
    Node::x = x;
}

unsigned int Node::getY() const {
    return y;
}

void Node::setY(unsigned int y) {
    Node::y = y;
}

int Node::getWidth() const {
    return width;
}

void Node::setWidth(unsigned int width) {
    Node::width = width;
}

unsigned int Node::getHeight() const {
    return height;
}

void Node::setHeight(unsigned int height) {
    Node::height = height;
}

unsigned int Node::getFontSize() const {
    return fontSize;
}

void Node::setFontSize(unsigned int fontSize) {
    Node::fontSize = fontSize;
}

void Node::setId(int id) {
    this->id = id;
}
int Node::getId() {
    return id;
}

void Node::removeNodeFromOutEdges(Node* node) {
    for (int i = 0; i < outEdges.size(); i++) {
        if (outEdges[i]->getTail()->getId() == node->getId()) {
            std::swap(outEdges[i], outEdges.back());
            outEdges.pop_back();
            return;
        }
    }
}

void Node::removeNodeFromInEdges(Node* node) {
    for (int i = 0; i < inEdges.size(); i++) {
        if (inEdges[i]->getHead()->getId() == node->getId()) {
            std::swap(inEdges[i], inEdges.back());
            inEdges.pop_back();
            return;
        }
    }
}

void Node::addOutEdge(Edge* edge) {
    outEdges.push_back(edge);
}

void Node::addInEdge(Edge* edge) {
    inEdges.push_back(edge);
}

const std::vector<Edge*>& Node::getInEdges() const {
    return inEdges;
}

const std::vector<Edge*>& Node::getOutEdges() const {
    return outEdges;
}

int Node::getIndex() const {
    return index;
}

void Node::setIndex(int index) {
    Node::index = index;
}

void Node::clearInEdges() {
    inEdges.clear();
}

void Node::clearOutEdges() {
    outEdges.clear();
}

int Node::getRank() const {
    return rank;
}

void Node::setRank(int rank) {
    this->rank = rank;
}

int Node::getOrder() {
    return order;
}

void Node::setOrder(int order) {
    this->order = order;
}

void Node::setPosition(int position) {
    this->position = position;
}

int Node::getPosition() {
    return position;
}

void Node::setIsDummy(bool isDummy) {
    this->isDummy = isDummy;
}

bool Node::getIsDummy() {
    return isDummy;
}
} // namespace arn