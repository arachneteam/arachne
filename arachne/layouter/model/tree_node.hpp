//
// Created by Łukasz Majcher on 06.09.2017.
//

#ifndef ARACHNE_TREE_NODE_H
#define ARACHNE_TREE_NODE_H

#include <vector>

namespace arn {
class TreeNode {
public:
    void setLow(int i);

    void setParent(TreeNode *pNode);

    int getId();

    std::vector<TreeNode*>& getEdges();

    void setLim(int i);

    void setId(int i);

    int getLow();

    int getLim()const;

    TreeNode* getParent();

    void removeNode(TreeNode *pNode);

private:
    int low;
    int lim;
    TreeNode* parent;
    int id;
    std::vector<TreeNode*> edges;
};
} // namespace arn

#endif // ARACHNE_TREE_NODE_H
