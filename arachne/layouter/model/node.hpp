//
// Created by Łukasz Majcher on 07.08.2017.
//

#ifndef ARACHNE_NODE_H
#define ARACHNE_NODE_H

#include "edge.hpp"
#include <string>
#include <vector>

namespace arn {

class Edge;

class Node {
public:
    Node(std::string name);
    Node(std::string name, std::string label);
    void setLabel(const std::string& label);
    const std::string& getLabel() const;
    const std::string& getName() const;
    void setX(unsigned int x);
    unsigned int getY() const;
    void setY(unsigned int y);
    unsigned int getX() const;
    int getWidth() const;
    void setWidth(unsigned int width);
    unsigned int getHeight() const;
    void setHeight(unsigned int height);
    unsigned int getFontSize() const;
    void setFontSize(unsigned int fontSize);
    void setId(int id);
    int getId();
    void removeNodeFromOutEdges(Node* pNode);
    void removeNodeFromInEdges(Node* pNode);
    void addInEdge(Edge* pEdge);
    void addOutEdge(Edge* pEdge);
    const std::vector<Edge*>& getInEdges() const;
    const std::vector<Edge*>& getOutEdges() const;

    void clearInEdges();

    void clearOutEdges();

    int getRank() const;

    void setRank(int i);

    int getOrder();

    void setOrder(int order);

    void setPosition(int position);

    int getPosition();

    void setIsDummy(bool isDummy);

    bool getIsDummy();

private:
    std::string label;
    std::string name;
    unsigned int x;
    unsigned int y;
    unsigned int width;
    unsigned int height;
    unsigned int fontSize = 14;
    int id;
    int index;
    int order;
    int position;

public:
    int getIndex() const;

    void setIndex(int index);

private:
    std::vector<Edge*> inEdges;
    std::vector<Edge*> outEdges;
    int rank;
    bool isDummy=false;

};

} // namespace arn

#endif // ARACHNE_NODE_H
