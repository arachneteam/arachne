//
// Created by Łukasz Majcher on 06.08.2017.
//

#include "graph_layout.hpp"

namespace arn {

Edge* GraphLayout::addEdge(std::string source, std::string target) {
    Node* sourceNode = getOrAddNode(source);
    Node* targetNode = getOrAddNode(target);

    Edge* edge = new Edge(sourceNode, targetNode);
    edge->setWeight(1);
    edge->setMinLen(1);
    edges.push_back(edge);
    return edge;
}

Node* GraphLayout::addNode(std::string name) {
    Node* node = new Node(name);
    node->setLabel(name);
    nameToNodeMap[name] = node;
    nodes.push_back(node);
    return node;
}

Node* GraphLayout::addNode(Node* node) {
    nameToNodeMap[node->getName()] = node;
    nodes.push_back(node);
    return node;
}

Edge* GraphLayout::addEdge(Edge* edge) {
    edges.push_back(edge);
    return edge;
}

Node* GraphLayout::getOrAddNode(std::string name) {
    if (nameToNodeMap.find(name) != nameToNodeMap.end()) {
        return nameToNodeMap[name];
    }
    return addNode(name);
}

const std::string& GraphLayout::getName() const {
    return name;
}

bool GraphLayout::isIsDigraph() const {
    return isDigraph;
}

const std::vector<Node*>& GraphLayout::getNodes() const {
    return nodes;
}

const std::vector<Edge*> GraphLayout::getEdges() const {
    return edges;
}

int GraphLayout::getWidth() const {
    return width;
}

void GraphLayout::setWidth(int width) {
    GraphLayout::width = width;
}

int GraphLayout::getHeight() const {
    return height;
}

void GraphLayout::setHeight(int height) {
    GraphLayout::height = height;
}

void GraphLayout::setIsDigraph(bool isDigraph) {
    GraphLayout::isDigraph = isDigraph;
}

void GraphLayout::setName(const std::string& name) {
    GraphLayout::name = name;
}

Node* GraphLayout::getNode(int id) {
    return nodes[id];
}

int GraphLayout::size() {
    return (int)nodes.size();
}

Node* GraphLayout::getNodeById(int id) {
    return nodes[id];
}

// not very effective way
Edge* GraphLayout::getEdge(int headId, int tailId) {
    Node* head = getNodeById(headId);
    for (auto edge : head->getOutEdges()) {
        if (edge->getTail()->getId() == tailId) {
            return edge;
        }
    }
    return nullptr;
}

// not very effective
void GraphLayout::removeEdge(Edge* edge) {
    for (int i = 0; i < edges.size(); i++) {
        if (edges[i] == edge) {
            std::swap(edges[i], edges.back());
            edges.pop_back();
        }
    }
}

Node* GraphLayout::addDummyNode(std::string name) {
    Node* node = new Node(name);
    node->setLabel(name);
    nameToNodeMap[name] = node;
    nodes.push_back(node);
    node->setId(nodes.size() - 1);
    node->setIsDummy(true);
    return node;
}

Edge* GraphLayout::addEdge(Node* head, Node* tail) {
    Edge* edge = new Edge(head, tail);
    edges.push_back(edge);
    return edge;
}

} // namespace arn