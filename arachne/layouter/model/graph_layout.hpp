//
// Created by Łukasz Majcher on 06.08.2017.
//

#ifndef ARACHNE_GRAPH_LAYOUT_H
#define ARACHNE_GRAPH_LAYOUT_H

#include <map>
#include <string>
#include <vector>

#include "arachne/layouter/model/edge.hpp"
#include "arachne/layouter/model/node.hpp"

namespace arn {
class GraphLayout {
    friend class CycleRemover;
    friend class GraphUtils;

public:
    Edge* addEdge(std::string source, std::string target);

    int getWidth() const;

    void setWidth(int width);

    int getHeight() const;

    void setHeight(int height);

    const std::string& getName() const;

    bool isIsDigraph() const;

    const std::vector<Node*>& getNodes() const;

    const std::vector<Edge*> getEdges() const;

    Node* getOrAddNode(std::string name);

    Node* addNode(std::string name);

    Node* addNode(Node* node);

    void setName(const std::string& name);

    void setIsDigraph(bool isDigraph);

    Node* getNode(int id);

    Edge* addEdge(Edge* edge);

    int size();

    Node* getNodeById(int id);

    Edge* getEdge(int headId, int tailId);

    void removeEdge(Edge* edge);

    Node* addDummyNode(std::string name);

    Edge* addEdge(Node* head, Node* tail);

private:
    std::vector<Edge*> edges;
    std::map<std::string, Node*> nameToNodeMap;
    std::vector<Node*> nodes;
    int width;
    int height;
    std::string name;
    bool isDigraph = false;
};
} // namespace arn

#endif // ARACHNE_GRAPH_LAYOUT_H
