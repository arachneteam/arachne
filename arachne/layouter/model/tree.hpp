//
// Created by Łukasz Majcher on 05.09.2017.
//

#ifndef ARACHNE_TREE_H
#define ARACHNE_TREE_H

#include "tree_node.hpp"
#include <map>
#include <set>
#include <vector>

namespace arn {
class Tree {
    //friend class TreeUtils;

public:
    Tree(int size);

    TreeNode* addNode(int id);

    TreeNode* getNode(int id);

    std::vector<TreeNode*> getNodes();

    bool containsNode(int id);

    void addEdge(int firstId, int secondId, double weight, int minLen);

    TreeNode* getRoot();

    void setRoot(TreeNode* root);

    int size();

    bool containsEdge(int firstId, int secondId);

    int getEdgeCutValue(int firstId, int secondId);

    void setEdgeCutValue(int firstId, int secondId, int value);

    std::set<std::pair<int, int>>& getEdges();

    void removeEdge(int firstId, int secondId);

    std::map<std::pair<int, int>, int> getCutValueEdges();

private:
    std::vector<TreeNode*> nodes;
    TreeNode* root;
    int nodeCounter = 0;
    std::set<std::pair<int, int>> edges;
    std::map<std::pair<int, int>, int> cutValues;
}; // namespace arn
} // namespace arn

#endif // ARACHNE_TREE_H
