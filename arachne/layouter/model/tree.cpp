//
// Created by Łukasz Majcher on 05.09.2017.
//

#include "tree.hpp"

namespace arn {

Tree::Tree(int size) : nodes(size) {
}

TreeNode* Tree::addNode(int id) {
    TreeNode* node = new TreeNode();
    node->setId(id);
    nodes[id] = node;
    nodeCounter++;
    return node;
}

TreeNode* Tree::getNode(int id) {
    return nodes[id];
}

// not effective way of doing that
std::vector<TreeNode*> Tree::getNodes() {
    std::vector<TreeNode*> result;
    for (auto node : nodes) {
        if (node != nullptr) {
            result.push_back(node);
        }
    }
    return result;
}

bool Tree::containsNode(int id) {
    return nodes[id] != nullptr;
}

void Tree::addEdge(int firstId, int secondId, double weight, int minLen) {
    if (firstId > secondId) {
        std::swap(firstId, secondId);
    }
    TreeNode* firsNode = nodes[firstId];
    TreeNode* secondNode = nodes[secondId];
    firsNode->getEdges().push_back(secondNode);
    secondNode->getEdges().push_back(firsNode);
    edges.insert(std::make_pair(firstId, secondId));
}

TreeNode* Tree::getRoot() {
    return root;
}

void Tree::setRoot(TreeNode* root) {
    this->root = root;
}

int Tree::size() {
    return nodeCounter;
}

bool Tree::containsEdge(int firstId, int secondId) {
    if (firstId > secondId) {
        std::swap(firstId, secondId);
    }
    return edges.find(std::make_pair(firstId, secondId)) != edges.end();
}

int Tree::getEdgeCutValue(int firstId, int secondId) {
    if (firstId > secondId) {
        std::swap(firstId, secondId);
    }
    return cutValues[std::make_pair(firstId, secondId)];
}

void Tree::setEdgeCutValue(int firstId, int secondId, int value) {
    if (firstId > secondId) {
        std::swap(firstId, secondId);
    }
    cutValues[std::make_pair(firstId, secondId)] = value;
}

std::set<std::pair<int, int>>& Tree::getEdges() {
    return edges;
};

void Tree::removeEdge(int firstId, int secondId) {
    if (firstId > secondId) {
        std::swap(firstId, secondId);
    }
    edges.erase(std::make_pair(firstId, secondId));
    cutValues.erase(std::make_pair(firstId, secondId));
    TreeNode* firstNode = nodes[firstId];
    TreeNode* secondNode = nodes[secondId];
    firstNode->removeNode(secondNode);
    secondNode->removeNode(firstNode);
}

std::map<std::pair<int, int>, int> Tree::getCutValueEdges() {
    return cutValues;
}

} // namespace arn