//
// Created by Łukasz Majcher on 07.08.2017.
//

#ifndef ARACHNE_EDGE_H
#define ARACHNE_EDGE_H

#include "node.hpp"
#include <string>

namespace arn {

class Node;

class Edge {
public:
    Edge(Node* pNode, Node* pNode1);

    Node* getHead() const;

    void setHead(Node* head);

    Node* getTail() const;

    void setTail(Node* tail);

    int getId() const;

    void setId(int id);

    const std::string& getLabel() const;

    void setLabel(const std::string& label);

    int getWeight() const;

    void setWeight(int weight);

    int getMinLen() const;

    void setMinLen(int minLen);

    void setInverted(bool i);

    bool isInverted();

private:
    Node* tail;
    Node* head;
    int id;
    std::string label;
    int weight;
    int minLen;
    bool inverted = false;
};

} // namespace arn

#endif // ARACHNE_EDGE_H
