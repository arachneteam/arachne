//
// Created by Łukasz Majcher on 07.08.2017.
//

#include "arachne/layouter/model/edge.hpp"

namespace arn {
Edge::Edge(Node* head, Node* tail) {
    this->head = head;
    this->tail = tail;
}

Node* Edge::getHead() const {
    return head;
}

void Edge::setHead(Node* head) {
    Edge::head = head;
}

Node* Edge::getTail() const {
    return tail;
}

void Edge::setTail(Node* tail) {
    Edge::tail = tail;
}

int Edge::getId() const {
    return id;
}

void Edge::setId(int id) {
    Edge::id = id;
}

const std::string& Edge::getLabel() const {
    return label;
}

void Edge::setLabel(const std::string& label) {
    Edge::label = label;
}

int Edge::getWeight() const {
    return weight;
}

void Edge::setWeight(int weight) {
    Edge::weight = weight;
}

int Edge::getMinLen() const {
    return minLen;
}

void Edge::setMinLen(int minLen) {
    Edge::minLen = minLen;
}

void Edge::setInverted(bool inverted) {
    this->inverted = inverted;
}

bool Edge::isInverted() {
    return inverted;
}
} // namespace arn
