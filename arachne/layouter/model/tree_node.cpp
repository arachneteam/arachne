//
// Created by Łukasz Majcher on 06.09.2017.
//

#include "tree_node.hpp"
#include <vector>

namespace arn {

void TreeNode::setLow(int low) {
    this->low = low;
}

void TreeNode::setParent(TreeNode* parent) {
    this->parent = parent;
}

int TreeNode::getId() {
    return id;
}

std::vector<TreeNode*>& TreeNode::getEdges() {
    return edges;
}

void TreeNode::setLim(int lim) {
    this->lim = lim;
}

void TreeNode::setId(int id) {
    this->id = id;
}

int TreeNode::getLow() {
    return low;
}

int TreeNode::getLim() const {
    return lim;
}

TreeNode* TreeNode::getParent() {
    return parent;
}

// not very effective
void TreeNode::removeNode(TreeNode* node) {
    for (int i = 0; i < edges.size(); i++) {
        if (edges[i]->getId() == node->getId()) {
            edges[i] = edges.back();
            edges.pop_back();
        }
    }
}
} // namespace arn