//
// Created by Łukasz Majcher on 30.08.2017.
//

#include "string_utils.hpp"
#include <functional>

namespace arn {

// trim from start
std::string& StringUtils::ltrim(std::string& s) {
    s.erase(s.begin(),
            std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
std::string& StringUtils::rtrim(std::string& s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace)))
                    .base(),
            s.end());
    return s;
}

// trim both ends
std::string& StringUtils::trim(std::string& s) {
    return ltrim(rtrim(s));
}

std::vector<std::string> StringUtils::split(const std::string& text, const std::string sep) {
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = text.find(sep, start)) != std::string::npos) {
        if (end != start) {
            tokens.push_back(text.substr(start, end - start));
        }
        start = end + sep.size();
    }
    if (end != start) {
        tokens.push_back(text.substr(start));
    }
    return tokens;
}

std::string StringUtils::join(std::vector<std::string> str, std::string s) {
    std::string result;
    if(str.empty()){
        return result;
    }
    result = str[0];
    for(int i=1; i<str.size(); i++){
        result = result + s + str[i];
    }
    return result;
}

} // namespace arn