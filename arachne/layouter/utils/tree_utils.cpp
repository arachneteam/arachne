//
// Created by Łukasz Majcher on 06.09.2017.
//

#include "tree_utils.hpp"
#include "graph_utils.hpp"
#include <algorithm>

namespace arn {

void TreeUtils::assignCutValues(arn::Tree& tree, arn::GraphLayout& graph) {
    std::vector<TreeNode*> nodes = tree.getNodes();
    std::sort(nodes.begin(), nodes.end(), [](const TreeNode* a, const TreeNode* b) -> bool {
        return a->getLim() < b->getLim();
    });

    for (auto node : nodes) {
        if (node->getParent() != nullptr) {
            TreeNode* parent = node->getParent();
            tree.setEdgeCutValue(node->getId(), parent->getId(),
                                 calculateCutValue(tree, graph, node));
        }
    }
}

void TreeUtils::assignPostorder(TreeNode* root, int size) {
    std::vector<bool> visited(size);
    dfs(visited, root, nullptr, 0);
}

int TreeUtils::dfs(std::vector<bool> visited, TreeNode* x, TreeNode* parent, int counter) {
    x->setLow(counter);
    x->setParent(parent);
    visited[x->getId()] = true;

    for (auto y : x->getEdges()) {
        if (!visited[y->getId()]) {
            counter = dfs(visited, y, x, counter);
        }
    }

    x->setLim(counter++);
    return counter;
}

int TreeUtils::calculateCutValue(Tree& tree, GraphLayout& graph, TreeNode* node) {
    TreeNode* parent = node->getParent();
    Edge* edge = graph.getEdge(node->getId(), parent->getId());
    bool fromTailToHead = true;
    int cutValue;

    if (edge == nullptr) {
        fromTailToHead = false;
        edge = graph.getEdge(parent->getId(), node->getId());
    }

    cutValue = edge->getWeight();

    Node* graphNode = graph.getNodeById(node->getId());

    for (auto edge : graphNode->getInEdges()) {
        Node* head = edge->getHead();
        if (head->getId() != parent->getId()) {
            cutValue += (!fromTailToHead) ? edge->getWeight() : -edge->getWeight();
            if (tree.containsEdge(node->getId(), head->getId())) {
                cutValue += (!fromTailToHead) ? -tree.getEdgeCutValue(node->getId(), head->getId())
                                              : tree.getEdgeCutValue(node->getId(), head->getId());
            }
        }
    }

    for (auto edge : graphNode->getOutEdges()) {
        Node* tail = edge->getTail();
        if (tail->getId() != parent->getId()) {
            cutValue += (fromTailToHead) ? edge->getWeight() : -edge->getWeight();
            if (tree.containsEdge(node->getId(), tail->getId())) {
                cutValue += (fromTailToHead) ? -tree.getEdgeCutValue(node->getId(), tail->getId())
                                             : tree.getEdgeCutValue(node->getId(), tail->getId());
            }
        }
    }

    return cutValue;
}

Edge* TreeUtils::leaveEdge(Tree tree, GraphLayout& graph) {
    for (auto edge : tree.getCutValueEdges()) {
        if (edge.second < 0) {
            Edge* gEdge = graph.getEdge(edge.first.first, edge.first.second);
            if (gEdge == nullptr) {
                gEdge = graph.getEdge(edge.first.second, edge.first.first);
            }
            return gEdge;
        }
    }
    return nullptr;
}

Edge* TreeUtils::enterEdge(Tree& tree, GraphLayout& graph, Edge* e) {
    bool flip = false;

    TreeNode* x = tree.getNode(e->getHead()->getId());
    TreeNode* y = tree.getNode(e->getTail()->getId());
    TreeNode* tail = x;

    if (x->getLim() > y->getLim()) {
        flip = true;
        tail = y;
    }

    Edge* result = nullptr;
    for (auto edge : graph.getEdges()) {
        if (flip == isDescendant(tree.getNode(edge->getHead()->getId()), tail) &&
            flip != isDescendant(tree.getNode(edge->getTail()->getId()), tail)) {
            if (result == nullptr || GraphUtils::slack(result) > GraphUtils::slack(edge)) {
                result = edge;
            }
        }
    }

    return result;
}

void TreeUtils::exchange(Tree& tree, GraphLayout& graph, Edge* e, Edge* f) {
    tree.removeEdge(e->getHead()->getId(), e->getTail()->getId());
    tree.addEdge(f->getHead()->getId(), f->getTail()->getId(), 0, 0);

    assignPostorder(tree.getRoot(), graph.size());
    assignCutValues(tree, graph);
    assignRank(graph, tree);
}

bool TreeUtils::isDescendant(TreeNode* node, TreeNode* root) {
    return root->getLow() <= node->getLim() && node->getLim() <= root->getLim();
}

void TreeUtils::normalizeRanks(GraphLayout& graph) {
    int minRank = std::numeric_limits<int>::max();

    for (auto node : graph.getNodes()) {
        minRank = std::min(minRank, node->getRank());
    }

    for (auto node : graph.getNodes()) {
        node->setRank(node->getRank() - minRank);
    }
}

void TreeUtils::assignRank(GraphLayout& graph, Tree& tree) {
    std::vector<TreeNode*> nodes = tree.getNodes();
    std::sort(nodes.begin(), nodes.end(), [](const TreeNode* a, const TreeNode* b) -> bool {
        return a->getLim() > b->getLim();
    });

    for (auto node : nodes) {
        if (node->getParent() != nullptr) {
            TreeNode* parent = node->getParent();
            Edge* edge = graph.getEdge(node->getId(), parent->getId());
            bool flip = false;

            if (edge == nullptr) {
                edge = graph.getEdge(parent->getId(), node->getId());
                flip = true;
            }

            int rank = graph.getNode(parent->getId())->getRank() +
                       (flip ? edge->getMinLen() : -edge->getMinLen());
            graph.getNode(node->getId())->setRank(rank);
        }
    }
}

void TreeUtils::removeEmptyRanks(GraphLayout& graph) {
    std::set<int> ranks;
    for (auto node : graph.getNodes()) {
        ranks.insert(node->getRank());
    }
    for (auto node : graph.getNodes()) {
        node->setRank(std::distance(ranks.begin(), ranks.find(node->getRank())));
    }
}

} // namespace arn