//
// Created by Łukasz Majcher on 03.09.2017.
//

#include "graph_utils.hpp"

namespace arn {
GraphLayout& GraphUtils::makeSimpleGraph(GraphLayout& graph) {
    std::map<std::pair<int, int>, Edge*> edgesMap;
    std::vector<Edge*> edgesToDelete;
    for (auto edge : graph.getEdges()) {
        int head_id = edge->getHead()->getId();
        int tail_id = edge->getTail()->getId();
        std::pair<int, int> key = std::make_pair(head_id, tail_id);

        if (edgesMap.find(key) != edgesMap.end()) {
            Edge* baseEdge = edgesMap[key];
            baseEdge->setWeight(baseEdge->getWeight() + edge->getWeight());
            baseEdge->setMinLen(std::max(baseEdge->getMinLen(), edge->getMinLen()));
            baseEdge->setLabel(baseEdge->getLabel() + "|" + edge->getLabel());
            edgesToDelete.push_back(edge);
        } else {
            edgesMap[key] = edge;
        }
    }

    graph.edges.clear();
    int id = 0;
    for (auto& entry : edgesMap) {
        entry.second->setId(id);
        graph.edges.push_back(entry.second);
        id++;
    }

    for (auto edge : edgesToDelete) {
        delete edge;
    }

    return graph;
}

GraphLayout& GraphUtils::setNodeIds(GraphLayout& graph) {
    for (int i = 0; i < graph.nodes.size(); i++) {
        graph.nodes[i]->setId(i);
    }
    return graph;
}

GraphLayout GraphUtils::setNodeInAndOutEdges(GraphLayout& graph) {
    for (auto node : graph.getNodes()) {
        node->clearInEdges();
        node->clearOutEdges();
    }

    for (auto edge : graph.edges) {
        edge->getHead()->addOutEdge(edge);
        edge->getTail()->addInEdge(edge);
    }
    return graph;
}

GraphLayout& GraphUtils::assignRank(GraphLayout& graph) {
    std::vector<bool> visited(graph.size());
    for (auto node : graph.getNodes()) {
        if (node->getInEdges().empty()) {
            dfs(node, visited);
        }
    }
    return graph;
}

int GraphUtils::dfs(Node* node, std::vector<bool>& visited) {
    if (visited[node->getId()]) {
        return node->getRank();
    }

    visited[node->getId()] = true;

    if (node->getOutEdges().empty()) {
        node->setRank(0);
        return 0;
    }

    int rank = std::numeric_limits<int>::max();
    for (auto edge : node->getOutEdges()) {
        rank = std::min(rank, dfs(edge->getTail(), visited) - edge->getMinLen());
    }

    node->setRank(rank);
    return rank;
}

int GraphUtils::slack(Edge* edge) {
    return edge->getTail()->getRank() - edge->getHead()->getRank() - edge->getMinLen();
}

GraphLayout& GraphUtils::normalizeEdges(GraphLayout& graph) {
    std::vector<Edge*> edges;
    for (auto edge : graph.getEdges()) {
        edges.push_back(edge);
    }
    int counter = 0;
    for (auto edge : graph.getEdges()) {
        counter = normalizeEdge(graph, edge, counter);
    }

    // normalize method can destroy in and out edges
    setNodeInAndOutEdges(graph);
    return graph;
}

int GraphUtils::normalizeEdge(GraphLayout& graph, Edge* edge, int counter) {
    Node* head = edge->getHead();
    Node* tail = edge->getTail();

    if (head->getRank() + 1 == tail->getRank()) {
        return counter;
    }

    graph.removeEdge(edge);
    Node* source = head;
    for (int rank = head->getRank() + 1; rank < tail->getRank(); rank++) {
        Node* dummy = graph.addDummyNode("dummy" + std::to_string(counter));
        counter++;
        dummy->setRank(rank);
        Edge* dummyEdge = graph.addEdge(source, dummy);
        dummyEdge->setWeight(edge->getWeight());
        dummyEdge->setMinLen(1);
        dummyEdge->setInverted(edge->isInverted());
        source = dummy;
    }
    Edge* dummyEdge = graph.addEdge(source, tail);
    dummyEdge->setWeight(edge->getWeight());
    dummyEdge->setMinLen(1);
    dummyEdge->setInverted(edge->isInverted());

    return counter;
}

std::vector<std::vector<Node*>> GraphUtils::toLayers(GraphLayout& graph) {
    int maxRank = 0;
    for (auto node : graph.getNodes()) {
        maxRank = std::max(maxRank, node->getRank());
    }
    std::vector<std::vector<Node*>> layers(maxRank+1);
    for (auto node : graph.getNodes()) {
        layers[node->getRank()].push_back(node);
    }

    return layers;
}
} // namespace arn