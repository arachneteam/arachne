//
// Created by Łukasz Majcher on 06.09.2017.
//

#ifndef ARACHNE_TREE_UTILS_H
#define ARACHNE_TREE_UTILS_H

#include <arachne/layouter/model/graph_layout.hpp>
#include <arachne/layouter/model/tree.hpp>

namespace arn {
class TreeUtils {
public:
    static void assignCutValues(arn::Tree& tree, arn::GraphLayout& layout);

    static void assignPostorder(TreeNode* root, int size);

    static int dfs(std::vector<bool> visited, TreeNode* x, TreeNode* parent, int counter);

    static int calculateCutValue(Tree& tree, GraphLayout& layout, TreeNode* pNode);

    static Edge *leaveEdge(Tree tree, GraphLayout &layout);

    static Edge* enterEdge(Tree& tree, GraphLayout& layout, Edge* pEdge);

    static void exchange(Tree& tree, GraphLayout& layout, Edge* pEdge, Edge* f);

    static bool isDescendant(TreeNode *pNode, TreeNode *pTreeNode);

    static void normalizeRanks(GraphLayout &layout);

    static void assignRank(GraphLayout &layout, Tree &tree);

    static void removeEmptyRanks(GraphLayout &layout);
};

} // namespace arn

#endif // ARACHNE_TREE_UTILS_H
