//
// Created by Łukasz Majcher on 30.08.2017.
//

#ifndef ARACHNE_STRING_UTILS_H
#define ARACHNE_STRING_UTILS_H

#include <string>
#include <vector>

namespace arn {
class StringUtils {
public:
    static std::string& trim(std::string& s);
    static std::string& ltrim(std::string& s);
    static std::string& rtrim(std::string& s);
    static std::vector<std::string> split(const std::string& text, const std::string sep);
    static std::string join(std::vector<std::string> str, std::string s);

};

} // namespace arn

#endif // ARACHNE_STRING_UTILS_H
