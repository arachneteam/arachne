//
// Created by Łukasz Majcher on 03.09.2017.
//

#ifndef ARACHNE_GRAPH_UTILS_H
#define ARACHNE_GRAPH_UTILS_H

#include <arachne/layouter/model/graph_layout.hpp>

namespace arn {
class GraphUtils {
public:
    static GraphLayout& makeSimpleGraph(GraphLayout& graph);

    static GraphLayout& setNodeIds(GraphLayout& layout);

    static GraphLayout setNodeInAndOutEdges(GraphLayout& layout);

    static GraphLayout& assignRank(GraphLayout& layout);

    static int dfs(Node* pNode, std::vector<bool>& vector);

    static int slack(Edge* pEdge);

    static GraphLayout& normalizeEdges(GraphLayout& layout);

    static int normalizeEdge(GraphLayout& layout, Edge* pEdge, int counter);

    static std::vector<std::vector<Node *>> toLayers(GraphLayout &layout);
};
} // namespace arn

#endif // ARACHNE_GRAPH_UTILS_H
