//
// Created by Łukasz Majcher on 07.09.2017.
//

#include "order.hpp"
#include <iostream>
#include <algorithm>

namespace arn {

int Order::MAX_ITERATION = 14;

std::vector<std::vector<Node*>> Order::ordering(GraphLayout& graph) {
    std::vector<std::vector<Node*>> order = initOrder(graph);
    std::vector<std::vector<Node*>> best = order;

    for (int i = 0; i < MAX_ITERATION; i++) {
        wmedian(order, i);
        transpose(order);
        if (crossing(best) > crossing(order)) {
            best = order;
        }
    }

    return best;
}

std::vector<std::vector<Node*>> Order::initOrder(GraphLayout& graph) {
    std::vector<Node*> nodes = graph.getNodes();
    std::sort(nodes.begin(), nodes.end(),
              [](const Node* a, const Node* b) -> bool { return a->getRank() < b->getRank(); });

    int maxRank = 0;
    for (auto node : graph.getNodes()) {
        maxRank = std::max(maxRank, node->getRank());
    }
    std::vector<std::vector<Node*>> layers(maxRank + 1);

    std::vector<bool> visited((unsigned long)graph.size());
    for (auto node : nodes) {
        if (!visited[node->getId()]) {
            dfs(node, visited, layers);
        }
    }

    return layers;
}

void Order::dfs(Node* node, std::vector<bool>& visited, std::vector<std::vector<Node*>>& layers) {
    visited[node->getId()] = true;
    layers[node->getRank()].push_back(node);
    node->setOrder((int)(layers[node->getRank()].size() - 1));
    for (auto edge : node->getOutEdges()) {
        Node* tail = edge->getTail();
        if (!visited[tail->getId()]) {
            dfs(tail, visited, layers);
        }
    }
}

void Order::wmedian(std::vector<std::vector<Node*>>& order, int iter) {
    if (iter % 2 == 0) {
        for (int r = 1; r < order.size(); r++) {
            std::vector<double> median;
            for (int v = 0; v < order[r].size(); v++) {
                median.push_back(medianValue(order[r][v], true));
            }
            sortLayer(median, r, order);
        }
    } else {
        for (int r = order.size() - 2; r >= 0; r--) {
            std::vector<double> median;
            for (int v = 0; v < order[r].size(); v++) {
                median.push_back(medianValue(order[r][v], false));
            }
            sortLayer(median, r, order);
        }
    }
}

double Order::medianValue(Node*& v, bool leftToRight) {
    std::vector<int> p = adjPosition(v, leftToRight);
    int m = (int)(p.size() / 2);
    if (p.empty()) {
        return -1.0;
    } else if (p.size() % 2 == 1) {
        return p[m];
    } else if (p.size() == 2) {
        return (p[0] + p[1]) / 2.0;
    } else {
        double left = p[m - 1] - p[0];
        double right = p[p.size() - 1] - p[m];
        return (p[m - 1] * right + p[m] * left) / (left + right);
    }
}

std::vector<int> Order::adjPosition(Node*& v, bool leftToRight) {
    std::vector<int> positions;
    if (leftToRight) {
        for (auto edge : v->getInEdges()) {
            Node* x = edge->getHead();
            positions.push_back(x->getOrder());
        }
    } else {
        for (auto edge : v->getOutEdges()) {
            Node* x = edge->getTail();
            positions.push_back(x->getOrder());
        }
    }

    std::sort(positions.begin(), positions.end());
    return positions;
}

void Order::sortLayer(std::vector<double> medians, int r, std::vector<std::vector<Node*>>& order) {
    std::vector<Node*> layer = order[r];
    std::vector<std::pair<double, int>> m;
    std::vector<int> singles;
    for (int i = 0; i < medians.size(); i++) {
        if (medians[i] > -0.5) {
            m.push_back(std::make_pair(medians[i], i));
        } else {
            singles.push_back(i);
        }
    }
    std::sort(m.begin(), m.end());

    singles.push_back(medians.size());
    int x = singles[0];
    int shift = 0;
    for (int i = 0; i < m.size(); i++) {
        while (i < x) {
            layer[i + shift] = order[r][m[i].second];
            layer[i + shift]->setOrder(i + shift);
            i++;
        }
        shift++;
        x = singles[shift];
    }
    order[r] = layer;
}

void Order::transpose(std::vector<std::vector<Node*>>& order) {
    bool improved = true;
    while (improved) {
        improved = false;
        for (int r = 0; r < order.size(); r++) {
            for (int i = 0; i < order[r].size() - 1; i++) {
                Node* v = order[r][i];
                Node* w = order[r][i + 1];
                if (crossing(v, w, order) > crossing(w, v, order)) {
                    improved = true;
                    exchange(order, v, w);
                }
            }
        }
    }
}

int Order::crossing(Node* v, Node* w, std::vector<std::vector<Node*>>& order) {
    int r = v->getRank();
    bool swap = false;
    if (v->getOrder() > w->getOrder()) {
        exchange(order, v, w);
        swap = true;
    }

    int result = 0;
    if (r > 0) {
        result += twoLayerCrossing(order[r - 1], order[r]);
    }
    if (r < order.size() - 1) {
        result += twoLayerCrossing(order[r], order[r + 1]);
    }

    if (swap) {
        exchange(order, v, w);
    }

    return result;
}

void Order::exchange(std::vector<std::vector<Node*>>& order, Node* v, Node* w) {
    int r = v->getRank();
    int orderV = v->getOrder();
    int orderW = w->getOrder();
    std::swap(order[r][orderV], order[r][orderW]);
    v->setOrder(orderW);
    w->setOrder(orderV);
}

int Order::twoLayerCrossing(std::vector<Node*>& left, std::vector<Node*>& right) {
    std::vector<Edge*> edges;
    for (auto node : left) {
        for (auto edge : node->getOutEdges()) {
            edges.push_back(edge);
        }
    }

    std::sort(edges.begin(), edges.end(), [](const Edge* a, const Edge* b) -> bool {
        if (a->getHead()->getOrder() < b->getHead()->getOrder()) {
            return true;
        }
        if (a->getHead()->getOrder() > b->getHead()->getOrder()) {
            return false;
        }
        if (a->getTail()->getOrder() < b->getTail()->getOrder()) {
            return true;
        }
        return a->getTail()->getOrder() <= b->getTail()->getOrder();
    });

    int firstIndex = 2;
    int q = right.size();
    while (firstIndex < q) {
        firstIndex *= 2;
    }
    int treeSize = 2 * firstIndex - 1;
    firstIndex--;
    std::vector<int> tree(treeSize);
    for (int i = 0; i < treeSize; i++) {
        tree[i] = 0;
    }

    int index = 0;
    int crossCount = 0;
    for (auto edge : edges) {
        index = edge->getTail()->getOrder() + firstIndex;
        tree[index]++;
        while (index > 0) {
            if (index % 2) {
                crossCount += tree[index + 1];
            }
            index = (index - 1) / 2;
            tree[index]++;
        }
    }

    return crossCount;
}

int Order::crossing(std::vector<std::vector<Node*>>& order) {
    for (int r = 0; r < order.size(); r++) {
        for (int i = 0; i < order[r].size(); i++) {
            order[r][i]->setOrder(i);
        }
    }
    int result = 0;
    for (int i = 1; i < order.size(); i++) {
        result += twoLayerCrossing(order[i - 1], order[i]);
    }
    return result;
}

} // namespace arn
