//
// Created by Łukasz Majcher on 07.09.2017.
//

#ifndef ARACHNE_ORDER_H
#define ARACHNE_ORDER_H

#include <arachne/layouter/model/graph_layout.hpp>

namespace arn {
class Order {
public:
    static int MAX_ITERATION;

    static std::vector<std::vector<Node*>> ordering(GraphLayout& graph);

    static std::vector<std::vector<Node*>> initOrder(GraphLayout& layout);

    static void dfs(Node* pNode, std::vector<bool>& vector,
                    std::vector<std::vector<Node*>>& layers);

    static void wmedian(std::vector<std::vector<Node*>>& order, int i);

    static double medianValue(Node*& v, bool b);

    static std::vector<int> adjPosition(Node*& v, bool b);

    static void sortLayer(std::vector<double> medians, int r,
                          std::vector<std::vector<Node*>>& order);

    static void transpose(std::vector<std::vector<Node*>>& order);

    static int crossing(Node* pNode, Node* w, std::vector<std::vector<Node*>>& vector);

    static void exchange(std::vector<std::vector<Node*>>& order, Node* v, Node* w);

    static int twoLayerCrossing(std::vector<Node*>& vector, std::vector<Node*>& vector1);

    static int crossing(std::vector<std::vector<Node*>>& order);
};
} // namespace arn

#endif // ARACHNE_ORDER_H
