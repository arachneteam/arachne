#ifndef ARACHNE_DFS_ORDER_VISITOR_HPP
#define ARACHNE_DFS_ORDER_VISITOR_HPP

#include <set>
#include <vector>

namespace arn {

template <typename V>
class DFSOrderVisitor {
public:
    bool visited(V v);
    void preorder(V v);

    const std::vector<V>& getOrder() const;

private:
    std::set<V> visited_v;
    std::vector<V> order;
};

template <typename V>
bool DFSOrderVisitor<V>::visited(V v) {
    auto it = visited_v.find(v);
    return it == visited_v.end();
}

template <typename V>
void DFSOrderVisitor<V>::preorder(V v) {
    order.push_back(v);
    visited_v.insert(v);
}

template <typename V>
const std::vector<V>& DFSOrderVisitor<V>::getOrder() const {
    return order;
}

} // namespace ach

#endif //ARACHNE_DFS_ORDER_VISITOR_HPP
