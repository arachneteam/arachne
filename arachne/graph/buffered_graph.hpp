//
// Created by mtwarog on 11/28/17.
//

#ifndef ARACHNE_BUFFERED_GRAPH_HPP
#define ARACHNE_BUFFERED_GRAPH_HPP

#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <boost/iterator/transform_iterator.hpp>
#include <boost/range/irange.hpp>

#include <arachne/graph/edge.hpp>
#include <arachne/utils/casting_iterator.hpp>
#include <arachne/utils/nested_collection_iterator.hpp>

namespace arn {
namespace detail {

template <typename T>
auto& first(const T& p) {
    return p.first;
}

template <typename T>
auto& second(const T& p) {
    return p.second;
}

} // namespace detail

template <typename V = uint_fast32_t, typename E = uint_fast32_t>
class BufferedGraph {
public:
    using vertex_index = V;
    using edge_index = E;
    using edge_type = DirectedEdge<vertex_index, edge_index>;

protected:
    using graph_map_type = std::unordered_map<vertex_index, std::vector<edge_type>>;
    using edge_map_type = std::unordered_map<edge_index, std::pair<std::size_t, std::size_t>>;
    using incoming_map_type = std::unordered_map<vertex_index, std::unordered_set<edge_index>>;

    using edge_map_it = typename edge_map_type::iterator;
    using graph_map_it = typename graph_map_type::iterator;
    using graph_map_cit = typename graph_map_type::const_iterator;
    using graph_map_cit_v = typename graph_map_cit::value_type;
    using inner_edge_it = CastingIterator<graph_map_cit, detail::second<graph_map_cit_v>>;
    using incoming_inner_it = typename incoming_map_type::mapped_type::iterator;
    struct IncomingHelper;

public:
    using vertex_iterator = CastingIterator<graph_map_cit, detail::first<graph_map_cit_v>>;
    using edge_iterator = NestedCollectionIterator<boost::iterator_range<inner_edge_it>>;
    using incoming_iterator =
            boost::iterators::transform_iterator<IncomingHelper, incoming_inner_it>;

    BufferedGraph() = default;

    void addVertex(vertex_index v);

    void addEdge(vertex_index u, vertex_index v);

    const std::vector<edge_type>& outgoing(vertex_index v) const;

    const boost::iterator_range<vertex_iterator> getVertices() const;

    const boost::iterator_range<edge_iterator> getEdges() const;

    const edge_type& getEdge(edge_index i) const;

    void removeEdge(edge_index i);

    void addVertices(const std::initializer_list<vertex_index>& list);

    template <typename Range>
    void addVertices(const Range& r);

    template <typename It>
    void addVertices(const It& begin, const It& end);

    boost::iterator_range<incoming_iterator> incoming(V v);

    void removeVertex(vertex_index v);

    void addEdges(const std::initializer_list<std::pair<vertex_index, vertex_index>> list);

    template <typename Range>
    void addEdges(const Range& r);

    template <typename It>
    void addEdges(const It& begin, const It& end);

private:
    vertex_index removeEdgePartial(edge_map_it&& it);

protected:
    graph_map_type G;
    edge_map_type edgeMap;
    incoming_map_type incomingMap;
    edge_index edgeCount = 0;

    struct IncomingHelper {
        using graph_type = BufferedGraph<V, E>;
        using result_type = const graph_type::edge_type&;

        IncomingHelper() = default;
        IncomingHelper(graph_type* graph);
        result_type operator()(const graph_type::edge_index& i) const;

    private:
        graph_type* graph;
    };
};

template <typename V, typename E>
void BufferedGraph<V, E>::addVertex(vertex_index v) {
    G.try_emplace(v, std::vector<edge_type>{});
    incomingMap.try_emplace(v, std::unordered_set<edge_index>{});
}

template <typename V, typename E>
void BufferedGraph<V, E>::addEdge(vertex_index u, vertex_index v) {
    std::vector<edge_type>& vec = G.at(u);
    std::unordered_set<edge_index>& inc = incomingMap.at(v);

    edgeMap.insert({edgeCount, {u, vec.size()}});
    vec.emplace_back(edgeCount, u, v);
    inc.insert(edgeCount++);
}

template <typename V, typename E>
auto BufferedGraph<V, E>::outgoing(vertex_index v) const -> const std::vector<edge_type>& {
    return G.at(v);
}

template <typename V, typename E>
auto BufferedGraph<V, E>::getVertices() const -> const boost::iterator_range<vertex_iterator> {
    return boost::iterator_range<vertex_iterator>(vertex_iterator(G.begin()),
                                                  vertex_iterator(G.end()));
}

template <typename V, typename E>
auto BufferedGraph<V, E>::getEdges() const -> const boost::iterator_range<edge_iterator> {
    boost::iterator_range<inner_edge_it> inner(inner_edge_it(G.begin()), inner_edge_it(G.end()));

    return boost::iterator_range<edge_iterator>(edge_iterator::begin(inner),
                                                edge_iterator::end(inner));
}

template <typename V, typename E>
auto BufferedGraph<V, E>::getEdge(edge_index i) const -> const edge_type& {
    auto p = edgeMap.at(i);
    return G.find(p.first)->second[p.second];
}

template <typename V, typename E>
template <typename Range>
void BufferedGraph<V, E>::addVertices(const Range& r) {
    addVertices(r.begin(), r.end());
}

template <typename V, typename E>
template <typename It>
void BufferedGraph<V, E>::addVertices(const It& begin, const It& end) {
    for (auto it = begin; it != end; ++it)
        this->addVertex(*it);
}

template <typename V, typename E>
void BufferedGraph<V, E>::addVertices(const std::initializer_list<vertex_index>& list) {
    addVertices(list.begin(), list.end());
}

template <typename V, typename E>
auto BufferedGraph<V, E>::removeEdgePartial(edge_map_it&& it) -> vertex_index {
    auto& p = it->second;

    std::vector<edge_type>& vec = G.find(p.first)->second;
    auto& edge = vec[p.second];

    edgeMap[vec.back().idx] = p;

    vertex_index ret = edge.end;

    std::swap(edge, vec.back());
    vec.pop_back();
    edgeMap.erase(it);

    return ret;
}

template <typename V, typename E>
void BufferedGraph<V, E>::removeEdge(edge_index i) {
    auto it = edgeMap.find(i);
    if (it == edgeMap.end())
        return;

    vertex_index edge_end = removeEdgePartial(std::move(it));
    incomingMap[edge_end].erase(i);
}

template <typename V, typename E>
auto BufferedGraph<V, E>::incoming(vertex_index v) -> boost::iterator_range<incoming_iterator> {
    IncomingHelper helper(this);
    incoming_iterator begin = boost::make_transform_iterator(std::begin(incomingMap.at(v)), helper);
    incoming_iterator end = boost::make_transform_iterator(std::end(incomingMap[v]), helper);

    return boost::make_iterator_range(begin, end);
}

template <typename V, typename E>
void BufferedGraph<V, E>::removeVertex(V v) {
    graph_map_it out_it = G.find(v);
    if (out_it == G.end())
        return;

    auto inc_it = incomingMap.find(v);
    std::unordered_set<edge_index>& inc = inc_it->second;

    for (edge_index i : inc)
        removeEdgePartial(edgeMap.find(i));

    incomingMap.erase(inc_it);
    G.erase(out_it);
}

template <typename V, typename E>
void BufferedGraph<V, E>::addEdges(
        const std::initializer_list<std::pair<vertex_index, vertex_index>> list) {
    addEdges(list.begin(), list.end());
}

template <typename V, typename E>
template <typename Range>
void BufferedGraph<V, E>::addEdges(const Range& r) {
    addEdges(r.begin(), r.end());
}

template <typename V, typename E>
template <typename It>
void BufferedGraph<V, E>::addEdges(const It& begin, const It& end) {
    for (auto it = begin; it != end; ++it)
        this->addEdge(it->first, it->second);
}

template <typename V, typename E>
BufferedGraph<V, E>::IncomingHelper::IncomingHelper(graph_type* graph) : graph(graph) {
}

template <typename V, typename E>
auto BufferedGraph<V, E>::IncomingHelper::operator()(const graph_type::edge_index& i) const
        -> result_type {
    return graph->getEdge(i);
}

} // namespace arn

#endif // ARACHNE_BUFFERED_GRAPH_HPP
