#ifndef ARACHNE_COMPACT_GRAPH_HPP
#define ARACHNE_COMPACT_GRAPH_HPP

#include <cstddef>
#include <cstdint>
#include <type_traits>
#include <vector>

#include <boost/range/irange.hpp>

#include <arachne/graph/edge.hpp>
#include <arachne/utils/nested_collection_iterator.hpp>

#include <iostream>

namespace arn {

template <typename V = uint_fast32_t, typename E = V>
class CompactGraph {
public:
    using vertex_index = V;
    using edge_index = E;
    using edge_type = DirectedEdge<vertex_index, edge_index>;
    using edge_iterator = NestedCollectionIterator<std::vector<std::vector<edge_type>>>;
    using vertex_iterator = typename boost::integer_range<vertex_index>::iterator;

public:
    CompactGraph();

    CompactGraph(std::size_t n);

    void setSize(std::size_t n);

    void addEdge(vertex_index u, vertex_index v);

    std::size_t getSize() const;

    const std::vector<edge_type>& outgoing(vertex_index v) const;

    const boost::integer_range<vertex_index> getVertices() const;

    const boost::iterator_range<edge_iterator> getEdges() const;

    // virtual ~CompactGraph() {}
protected:
    std::vector<std::vector<edge_type>> G;
    edge_index edgeCount = 0;
};

template <typename V, typename E>
CompactGraph<V, E>::CompactGraph() {
}

template <typename V, typename E>
CompactGraph<V, E>::CompactGraph(std::size_t n) : G(n) {
}

template <typename V, typename E>
void CompactGraph<V, E>::setSize(std::size_t n) {
    G.resize(n);
}

template <typename V, typename E>
std::size_t CompactGraph<V, E>::getSize() const {
    return G.size();
}

template <typename V, typename E>
void CompactGraph<V, E>::addEdge(vertex_index u, vertex_index v) {
    G[u].push_back(edge_type(edgeCount++, u, v));
}

template <typename V, typename E>
auto CompactGraph<V, E>::outgoing(vertex_index v) const -> const std::vector<edge_type>& {
    return G[v];
}

template <typename V, typename E>
auto CompactGraph<V, E>::getVertices() const -> const boost::integer_range<vertex_index> {
    return boost::integer_range<vertex_index>(vertex_index(0), vertex_index(G.size()));
}

template <typename V, typename E>
auto CompactGraph<V, E>::getEdges() const -> const boost::iterator_range<edge_iterator> {
    return boost::iterator_range<edge_iterator>(edge_iterator::begin(G), edge_iterator::end(G));
}

} // namespace arn

#endif // ARACHNE_COMPACT_GRAPH_HPP
