#ifndef ARACHNE_EDGE_HPP
#define ARACHNE_EDGE_HPP

#include <iostream>

namespace arn {

template <typename V, typename E>
class DirectedEdge {
public:
    using vertex_index = V;
    using edge_index = E;

    DirectedEdge(edge_index idx, vertex_index begin, vertex_index end);

    bool operator==(const DirectedEdge<V, E>& rhs) const;

    bool operator!=(const DirectedEdge<V, E>& rhs) const;

    template <typename W, typename F>
    friend std::ostream& operator<<(std::ostream& os, const DirectedEdge<W, F>& e);

    edge_index idx;
    vertex_index begin;
    vertex_index end;
};

template <typename V, typename E>
DirectedEdge<V, E>::DirectedEdge(edge_index idx, vertex_index begin, vertex_index end)
        : idx(idx), begin(begin), end(end) {
}

template <typename V, typename E>
bool DirectedEdge<V, E>::operator==(const DirectedEdge<V, E>& rhs) const {
    return this->idx == rhs.idx && this->begin == rhs.begin && this->end == rhs.end;
}

template <typename V, typename E>
bool DirectedEdge<V, E>::operator!=(const DirectedEdge<V, E>& rhs) const {
	return !(*this == rhs);
}

template <typename W, typename F>
std::ostream& operator<<(std::ostream& os, const DirectedEdge<W, F>& e) {
	os<<e.idx<<": "<<e.begin<<"->"<<e.end;
	return os;
};

}

#endif // ARACHNE_EDGE_HPP
