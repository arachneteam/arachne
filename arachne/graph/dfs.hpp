#ifndef ARACHNE_DFS_HPP
#define ARACHNE_DFS_HPP

#include <arachne/graph/dfs_order_visitor.hpp>
#include <arachne/utils/function_ptr.hpp>

namespace arn {

namespace detail {

namespace {
template <int N>
struct DFSMatcher : DFSMatcher<N - 1> {};

template <>
struct DFSMatcher<0> {};

template <typename Visitor, typename V>
void preorder(Visitor& visitor, V v) {
    visitor.preorder(v);
}

template <typename Visitor, typename V>
void postorder(Visitor& visitor, V v) {
    visitor.postorder(v);
}

template <typename Visitor, typename V>
void empty(Visitor& visitor, V v) {
}
}

template <typename Visitor, typename V, FunctionPtr<void, Visitor&, V> PreOrder,
          FunctionPtr<void, Visitor&, V> PostOrder, typename Graph>
auto DFSImpl(Graph& graph, Visitor& visitor, V v) {
    if (!visitor.visited(v))
        return;

    PreOrder(visitor, v);

    auto edges = graph.outgoing(v);

    for (auto edge : edges) {
        DFSImpl<Visitor, V, PreOrder, PostOrder>(graph, visitor, edge.end);
    }

    PostOrder(visitor, v);
}

template <typename Graph, typename Visitor, typename V>
auto matchDFS(Graph& graph, Visitor& visitor, V v, DFSMatcher<1>)
        -> decltype(visitor.preorder(v), visitor.postorder(v), void()) {
    return DFSImpl<Visitor, V, preorder, postorder>(graph, visitor, v);
}

template <typename Graph, typename Visitor, typename V>
auto matchDFS(Graph& graph, Visitor& visitor, V v, DFSMatcher<0>)
        -> decltype(visitor.preorder(v), void()) {
    return DFSImpl<Visitor, V, preorder, empty>(graph, visitor, v);
}

template <typename Graph, typename Visitor, typename V>
auto matchDFS(Graph& graph, Visitor& visitor, V v, DFSMatcher<0>)
        -> decltype(visitor.postorder(v), void()) {
    return DFSImpl<Visitor, V, empty, postorder>(graph, visitor, v);
}
}

template <typename Graph, typename Visitor, typename V>
void DFS(Graph& graph, Visitor& visitor, V v) {
    detail::matchDFS(graph, visitor, v, detail::DFSMatcher<1>());
};

template <typename Graph, typename Visitor, typename V>
void DFS(Graph& graph, Visitor&& visitor, V v) {
    DFS(graph, visitor, v);
};

template <typename Graph, typename V>
auto DFSOrder(Graph& graph, V v) {
    DFSOrderVisitor<V> visitor;
    DFS(graph, visitor, v);
    return visitor.getOrder();
};

} // namespace ach

#endif //ARACHNE_DFS_HPP
