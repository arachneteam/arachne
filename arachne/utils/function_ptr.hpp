//
// Created by mtwarog on 10/3/17.
//

#ifndef ARACHNE_FUNCTION_PTR_HPP
#define ARACHNE_FUNCTION_PTR_HPP

namespace arn {
template <typename Ret, typename... Args>
using FunctionPtr = Ret (*)(Args...);
}

#endif // ARACHNE_FUNCTION_PTR_HPP
