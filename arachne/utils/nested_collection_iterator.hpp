//
// Created by mtwarog on 12/1/17.
//

#ifndef ARACHNE_NESTED_COLLECTION_ITERATOR_HPP
#define ARACHNE_NESTED_COLLECTION_ITERATOR_HPP

#include <iterator>
#include <type_traits>

namespace arn {
template <typename T>
class NestedCollectionIterator {
private:
    // maybe allow non const
    //using outer_it_t = typename T::const_iterator;
  	using outer_it_t = decltype(std::cbegin(std::declval<T>()));
    //using inner_it_t = typename std::iterator_traits<outer_it_t>::value_type::const_iterator;
  	using inner_it_t = decltype(std::begin(std::declval<typename std::iterator_traits<outer_it_t>::value_type>()));
    
public:
    using difference_type = typename std::iterator_traits<inner_it_t>::difference_type;
    using value_type = typename std::iterator_traits<inner_it_t>::value_type;
    using pointer = typename std::iterator_traits<inner_it_t>::pointer;
    using reference = typename std::iterator_traits<inner_it_t>::reference;
    using iterator_category = std::input_iterator_tag;

    static NestedCollectionIterator begin(const T& v);

    static NestedCollectionIterator end(const T& v);

    NestedCollectionIterator() = default;

    NestedCollectionIterator(const NestedCollectionIterator<T>& b);

    reference operator*();

    pointer operator->();

    NestedCollectionIterator<T>& operator++();

    NestedCollectionIterator<T> operator++(int);

    bool operator==(const NestedCollectionIterator<T>& b) const;

    bool operator!=(const NestedCollectionIterator<T>& b) const;

private:
    NestedCollectionIterator(const outer_it_t& mainIt, const outer_it_t& mainItEnd);
    void advance();

    outer_it_t outerIt;
    outer_it_t outerItEnd;
    inner_it_t innerIt;
};

template <typename T>
NestedCollectionIterator<T> NestedCollectionIterator<T>::begin(const T& v) {
    NestedCollectionIterator<T> tmp(v.begin(), v.end());
    tmp.innerIt = tmp.outerIt->begin();
	tmp.advance();

    return tmp;
}

template <typename T>
NestedCollectionIterator<T> NestedCollectionIterator<T>::end(const T& v) {
    return NestedCollectionIterator<T>(v.end(), v.end());
}

template <typename T>
NestedCollectionIterator<T>::NestedCollectionIterator(const NestedCollectionIterator<T>& b)
        : outerIt(b.outerIt), outerItEnd(b.outerItEnd), innerIt(b.innerIt) {
}

template <typename T>
auto NestedCollectionIterator<T>::operator*() -> reference {
    return *innerIt;
}

template <typename T>
NestedCollectionIterator<T>& NestedCollectionIterator<T>::operator++() {
    // UB on end iterator
    innerIt++;
    advance();

    return *this;
}

template <typename T>
NestedCollectionIterator<T> NestedCollectionIterator<T>::operator++(int) {
    NestedCollectionIterator<T> tmp(*this);

    this->operator++();

    return tmp;
}

template <typename T>
bool NestedCollectionIterator<T>::operator!=(const NestedCollectionIterator<T>& b) const {
    return !(*this == b);
}

template <typename T>
bool NestedCollectionIterator<T>::operator==(const NestedCollectionIterator<T>& b) const {
    return outerIt == b.outerIt && outerItEnd == b.outerItEnd &&
           (outerIt == outerItEnd || innerIt == b.innerIt);
}

template <typename T>
auto NestedCollectionIterator<T>::operator-> () -> pointer {
    return innerIt.operator->();
}

template <typename T>
NestedCollectionIterator<T>::NestedCollectionIterator(const outer_it_t& mainIt,
                                                         const outer_it_t& mainItEnd)
        : outerIt(mainIt), outerItEnd(mainItEnd) {
}

template <typename T>
void NestedCollectionIterator<T>::advance() {
    while (outerIt != outerItEnd && innerIt == outerIt->end()) {
        ++outerIt;
        if (outerIt != outerItEnd) {
            innerIt = outerIt->begin();
        }
    }
}
}

#endif // ARACHNE_NESTED_COLLECTION_ITERATOR_HPP
