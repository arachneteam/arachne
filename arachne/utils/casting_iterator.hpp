//
// Created by mtwarog on 12/1/17.
//

#ifndef ARACHNE_CASTING_ITERATOR_HPP
#define ARACHNE_CASTING_ITERATOR_HPP

#include <iterator>
#include <type_traits>

namespace arn {

template <typename It, auto Fun>
class CastingIterator {
public:
    using difference_type = typename std::iterator_traits<It>::difference_type;
    using reference = decltype(Fun(std::declval<It>().operator*()));
    using value_type = typename std::remove_reference<reference>::type;
    using pointer = value_type*;
    using iterator_category = std::input_iterator_tag;

    CastingIterator() = default;

    CastingIterator(It it);

    reference operator*();

    pointer operator->();

    CastingIterator<It, Fun>& operator++();

    CastingIterator<It, Fun> operator++(int);

    bool operator!=(const CastingIterator<It, Fun>& b) const;

    bool operator==(const CastingIterator<It, Fun>& b) const;

private:
    It it;
};

template <typename It, auto Fun>
CastingIterator<It, Fun>::CastingIterator(It it) : it(it) {
}

template <typename It, auto Fun>
auto CastingIterator<It, Fun>::operator*() -> reference {
    return Fun(*it);
}

template <typename It, auto Fun>
auto CastingIterator<It, Fun>::operator-> () -> pointer {
    return &(this->operator*());
}

template <typename It, auto Fun>
CastingIterator<It, Fun>& CastingIterator<It, Fun>::operator++() {
    ++it;
    return *this;
}

template <typename It, auto Fun>
CastingIterator<It, Fun> CastingIterator<It, Fun>::operator++(int) {
    CastingIterator tmp(it++);
    return tmp;
}

template <typename It, auto Fun>
bool CastingIterator<It, Fun>::operator!=(const CastingIterator<It, Fun>& b) const {
    return !(*this == b);
}

template <typename It, auto Fun>
bool CastingIterator<It, Fun>::operator==(const CastingIterator<It, Fun>& b) const {
    return it == b.it;
}
}

#endif // ARACHNE_CASTING_ITERATOR_HPP
