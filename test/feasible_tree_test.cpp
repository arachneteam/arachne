//
// Created by Łukasz Majcher on 06.09.2017.
//

#include <arachne/layouter/layouters/layouter.hpp>
#include <arachne/layouter/model/tree.hpp>
#include <arachne/layouter/rank/feasible_tree.hpp>
#include <arachne/layouter/utils/graph_utils.hpp>
#include <gtest/gtest.h>

namespace arn {
namespace testing {
class FeasibleTreeTest : public ::testing::Test {};

TEST_F(FeasibleTreeTest, should_create_feasbile_tree) {
    GraphLayout graph;
    graph.addNode("a");
    graph.addNode("b");
    graph.addNode("c");
    graph.addNode("d");
    Edge* edge1 = graph.addEdge("a", "b");
    edge1->setLabel("ab");
    edge1->setMinLen(1);
    edge1->setWeight(1);

    Edge* edge2 = graph.addEdge("b", "c");
    edge2->setLabel("bc");
    edge2->setMinLen(1);
    edge2->setWeight(1);

    Edge* edge3 = graph.addEdge("a", "c");
    edge3->setLabel("ac");
    edge3->setMinLen(3);
    edge3->setWeight(1);

    Edge* edge4 = graph.addEdge("c", "d");
    edge4->setLabel("cd");
    edge4->setMinLen(1);
    edge4->setWeight(1);

    graph = GraphUtils::setNodeIds(graph);
    graph = GraphUtils::setNodeInAndOutEdges(graph);
    graph = GraphUtils::assignRank(graph);

    Tree tree = FeasibleTree::findFeasibleTree(graph);

    std::vector<int> expectedRanks = {-4, -2, -1, 0};
    std::vector<int> actualRanks;
    for (auto node : graph.getNodes()) {
        actualRanks.push_back(node->getRank());
    }
    ASSERT_EQ(actualRanks.size(), expectedRanks.size());
    for (auto i = 0; i < expectedRanks.size(); i++) {
        ASSERT_EQ(actualRanks[i], expectedRanks[i]);
    }

    std::vector<int> expectedNodes = {0, 1, 2, 3};
    std::vector<int> actualNodes;
    for (auto node : tree.getNodes()) {
        actualNodes.push_back(node->getId());
    }
    ASSERT_EQ(actualNodes.size(), expectedNodes.size());
    for (auto i = 0; i < expectedNodes.size(); i++) {
        ASSERT_EQ(actualNodes[i], expectedNodes[i]);
    }
}

} // namespace testing
} // namespace arn