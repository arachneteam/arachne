//
// Created by mtwarog on 11/27/17.
//
#include <arachne/graph/compact_graph.hpp>
#include <arachne/graph/dfs.hpp>

#include <gmock/gmock-matchers.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace arn {
namespace testing {

using ::testing::ElementsAre;
using g = CompactGraph<>;
using e = g::edge_type;

class CompactGraphTest : public ::testing::Test {};

TEST_F(CompactGraphTest, vertices_test) {
    CompactGraph<> g(5);

    EXPECT_EQ(5, g.getSize());
    EXPECT_THAT(g.getVertices(), ElementsAre(0, 1, 2, 3, 4));

    CompactGraph<> f;

    EXPECT_EQ(0, f.getSize());
    EXPECT_THAT(f.getVertices(), ElementsAre());

    f.setSize(3);

    EXPECT_EQ(3, f.getSize());
    EXPECT_THAT(f.getVertices(), ElementsAre(0, 1, 2));
}

TEST_F(CompactGraphTest, edges_test) {
    CompactGraph<> g(5);

    g.addEdge(0, 1);
    g.addEdge(1, 2);
    g.addEdge(2, 4);
    g.addEdge(2, 3);
    
    EXPECT_THAT(g.getEdges(), ElementsAre(e(0, 0, 1), e(1, 1, 2), e(2, 2, 4), e(3, 2, 3)));
}
}
}