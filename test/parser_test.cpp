//
// Created by Łukasz Majcher on 23.08.2017.
//

#include <arachne/layouter/parser/parse_exception.h>
#include <arachne/layouter/parser/parser.h>
#include <gtest/gtest.h>

namespace arn {
namespace testing {

class ParserTest : public ::testing::Test {};

TEST_F(ParserTest, should_parse_simple_undirected_graph) {
    arn::Parser parser;

    GraphLayout graphLayout = parser.parse("test_one.txt");
    ASSERT_FALSE(graphLayout.isIsDigraph());
    ASSERT_EQ(graphLayout.getName(), "graphname");

    std::vector<std::string> expectedNodes = {"a", "b", "c", "d", "e", "f", "g", "h"};
    std::vector<std::string> actualNodes;
    for (Node* node : graphLayout.getNodes()) {
        actualNodes.push_back(node->getName());
    }
    ASSERT_EQ(actualNodes.size(), expectedNodes.size());
    for (auto i = 0; i < expectedNodes.size(); i++) {
        ASSERT_EQ(actualNodes[i], expectedNodes[i]);
    }

    std::vector<std::string> expectedEdges = {"a--b", "b--a", "b--c", "c--b", "b--d",
                                              "d--b", "d--e", "e--d", "e--f", "f--e",
                                              "g--h", "h--g", "a--h", "h--a"};
    std::vector<std::string> actualEdges;
    for (Edge* edge : graphLayout.getEdges()) {
        actualEdges.push_back(edge->getHead()->getName() + std::string("--") + edge->getTail()->getName());
    }
    ASSERT_EQ(actualEdges.size(), expectedEdges.size());
    for (auto i = 0; i < expectedEdges.size(); i++) {
        ASSERT_EQ(actualEdges[i], expectedEdges[i]);
    }
}

TEST_F(ParserTest, should_parse_simple_directed_graph) {
    arn::Parser parser;

    GraphLayout graphLayout = parser.parse("test_two.txt");
    ASSERT_TRUE(graphLayout.isIsDigraph());
    ASSERT_EQ(graphLayout.getName(), "dag");

    std::vector<std::string> expectedNodes = {"a", "b", "c", "d", "e", "f", "g", "h"};
    std::vector<std::string> actualNodes;
    for (Node* node : graphLayout.getNodes()) {
        actualNodes.push_back(node->getName());
    }
    ASSERT_EQ(actualNodes.size(), expectedNodes.size());
    for (auto i = 0; i < expectedNodes.size(); i++) {
        ASSERT_EQ(actualNodes[i], expectedNodes[i]);
    }

    std::vector<std::string> expectedEdges = {"a--b", "b--c", "c--d", "d--e",
                                              "e--f", "g--h", "h--a", "f--g"};
    std::vector<std::string> actualEdges;
    for (Edge* edge : graphLayout.getEdges()) {
        actualEdges.push_back(edge->getHead()->getName() + std::string("--") + edge->getTail()->getName());
    }
    ASSERT_EQ(actualEdges.size(), expectedEdges.size());
    for (auto i = 0; i < expectedEdges.size(); i++) {
        ASSERT_EQ(actualEdges[i], expectedEdges[i]);
    }
}

TEST_F(ParserTest, should_throw_parse_error) {
    arn::Parser parser;

    try {
        parser.parse("test_three.txt");
    } catch (const ParseException& exception) {
        EXPECT_EQ(std::string("Invalid node id: "), exception.getMessage());
    }
}

TEST_F(ParserTest, should_throw_parse_error_invalid_edge) {
    arn::Parser parser;

   try {
        parser.parse("test_four.txt");
   } catch (const ParseException& exception) {
        EXPECT_EQ("Invalid node id: c&^$", exception.getMessage());
   }
}

TEST_F(ParserTest, should_parse_node_with_labels) {
    arn::Parser parser;

    GraphLayout graph = parser.parse("test_five.txt");

    ASSERT_FALSE(graph.isIsDigraph());
    ASSERT_EQ(graph.getName(), "g");

    std::vector<std::string> expectedNodes = {"a", "b", "c", "d"};
    std::vector<std::string> actualNodes;
    for (Node* node : graph.getNodes()) {
        actualNodes.push_back(node->getName());
    }

    ASSERT_EQ(actualNodes.size(), expectedNodes.size());
    for (auto i = 0; i < expectedNodes.size(); i++) {
        ASSERT_EQ(actualNodes[i], expectedNodes[i]);
    }

    std::vector<std::string> expectedLabels = {"a_label", "b", "c", "d_label"};
    std::vector<std::string> actualLabels;
    for (Node* node : graph.getNodes()) {
        actualLabels.push_back(node->getLabel());
    }

    ASSERT_EQ(actualLabels.size(), expectedLabels.size());
    for (auto i = 0; i < expectedLabels.size(); i++) {
        ASSERT_EQ(actualLabels[i], expectedLabels[i]);
    }

    std::vector<std::string> expectedEdges = {"a--b", "b--a"};
    std::vector<std::string> actualEdges;
    for (Edge* edge : graph.getEdges()) {
        actualEdges.push_back(edge->getHead()->getName() + std::string("--") + edge->getTail()->getName());
    }
    ASSERT_EQ(actualEdges.size(), expectedEdges.size());
    for (auto i = 0; i < expectedEdges.size(); i++) {
        ASSERT_EQ(actualEdges[i], expectedEdges[i]);
    }
}

    TEST_F(ParserTest, should_parse_node_with_complex_labels) {
        arn::Parser parser;

        GraphLayout graph = parser.parse("test_seven.txt");

        ASSERT_FALSE(graph.isIsDigraph());
        ASSERT_EQ(graph.getName(), "g");

        std::vector<std::string> expectedNodes = {"a", "b", "c", "d"};
        std::vector<std::string> actualNodes;
        for (Node* node : graph.getNodes()) {
            actualNodes.push_back(node->getName());
        }

        ASSERT_EQ(actualNodes.size(), expectedNodes.size());
        for (auto i = 0; i < expectedNodes.size(); i++) {
            ASSERT_EQ(actualNodes[i], expectedNodes[i]);
        }

        std::vector<std::string> expectedLabels = {"a_label", "first\nsecond\nthird", "first second third", "d_label"};
        std::vector<std::string> actualLabels;
        for (Node* node : graph.getNodes()) {
            actualLabels.push_back(node->getLabel());
        }

        ASSERT_EQ(actualLabels.size(), expectedLabels.size());
        for (auto i = 0; i < expectedLabels.size(); i++) {
            ASSERT_EQ(actualLabels[i], expectedLabels[i]);
        }

        std::vector<std::string> expectedEdges = {"a--b", "b--a"};
        std::vector<std::string> actualEdges;
        for (Edge* edge : graph.getEdges()) {
            actualEdges.push_back(edge->getHead()->getName() + std::string("--") + edge->getTail()->getName());
        }
        ASSERT_EQ(actualEdges.size(), expectedEdges.size());
        for (auto i = 0; i < expectedEdges.size(); i++) {
            ASSERT_EQ(actualEdges[i], expectedEdges[i]);
        }
    }
} // namespace testing
} // namespace arn
