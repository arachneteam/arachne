//
// Created by Łukasz Majcher on 05.09.2017.
//

#include <arachne/layouter/converter/element.hpp>
#include <arachne/layouter/converter/svg_converter.hpp>
#include <arachne/layouter/parser/parser.h>
#include <arachne/layouter/rank/cycle_remover.hpp>
#include <gtest/gtest.h>

namespace arn {
namespace testing {
class CycleRemoverTest : public ::testing::Test {};

TEST_F(CycleRemoverTest, should_remove_cycle) {
    Parser parser;
    GraphLayout graph = parser.parse("cycle.txt");
    graph = CycleRemover::execute(graph);

    std::vector<std::string> expectedEdges = {"a->b", "b->c", "c->e", "e->d", "d->a", "d->b"};
    std::vector<std::string> actualEdges;
    for (Edge* edge : graph.getEdges()) {
        actualEdges.push_back(edge->getHead()->getName() + std::string("->") +
                              edge->getTail()->getName());
    }
    ASSERT_EQ(actualEdges.size(), expectedEdges.size());
    for (auto i = 0; i < expectedEdges.size(); i++) {
        ASSERT_EQ(actualEdges[i], expectedEdges[i]);
    }
}

TEST_F(CycleRemoverTest, should_remove_cycle_from_clique) {
    Parser parser;
    GraphLayout graph = parser.parse("clique.txt");
    graph = CycleRemover::execute(graph);

    std::vector<std::string> expectedEdges = {"a->b", "b->a", "b->c", "c->b", "c->d",
                                              "d->c", "d->a", "a->d", "a->c", "c->a", "b->d", "d->b"};
    std::vector<std::string> actualEdges;
    for (Edge* edge : graph.getEdges()) {
        actualEdges.push_back(edge->getHead()->getName() + std::string("->") +
                              edge->getTail()->getName());
    }
    ASSERT_EQ(actualEdges.size(), expectedEdges.size());
    for (auto i = 0; i < expectedEdges.size(); i++) {
        ASSERT_EQ(actualEdges[i], expectedEdges[i]);
    }

    SvgConverter converter;
    converter.convert(graph, "output.svg");
}
} // namespace testing
} // namespace arn
