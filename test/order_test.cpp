//
// Created by Łukasz Majcher on 08.09.2017.
//

#include <arachne/layouter/layouters/layouter.hpp>
#include <arachne/layouter/order/order.hpp>
#include <arachne/layouter/utils/graph_utils.hpp>
#include <gtest/gtest.h>

namespace arn {
namespace testing {
class OrderTest : public ::testing::Test {};

void addEdge(GraphLayout& graph, std::string source, std::string target, std::string label,
             int minLen, int weight) {
    Edge* edge1 = graph.addEdge(source, target);
    edge1->setLabel(label);
    edge1->setMinLen(minLen);
    edge1->setWeight(weight);
}

TEST_F(OrderTest, should_create_init_order) {
    GraphLayout graph;
    graph.addNode("a")->setRank(0);
    graph.addNode("b")->setRank(2);
    graph.addNode("c")->setRank(3);
    Edge* edge1 = graph.addEdge("a", "b");
    edge1->setLabel("ab");
    edge1->setMinLen(1);
    edge1->setWeight(1);

    Edge* edge2 = graph.addEdge("b", "c");
    edge2->setLabel("bc");
    edge2->setMinLen(1);
    edge2->setWeight(1);

    Edge* edge3 = graph.addEdge("a", "c");
    edge3->setLabel("ac");
    edge3->setMinLen(3);
    edge3->setWeight(1);

    GraphUtils::setNodeIds(graph);
    std::vector<std::vector<Node*>> layers = Order::initOrder(graph);

    ASSERT_EQ(layers[0].back()->getLabel(), "a");
    ASSERT_EQ(layers[2].back()->getLabel(), "b");
    ASSERT_EQ(layers[3].back()->getLabel(), "c");
    ASSERT_TRUE(layers[1].empty());
}

TEST_F(OrderTest, should_count_cross_edges_between_two_layers) {
    GraphLayout graph;
    graph.addNode("a")->setRank(0);
    graph.addNode("b")->setRank(0);
    graph.addNode("c")->setRank(0);
    graph.addNode("d")->setRank(1);
    graph.addNode("e")->setRank(1);
    graph.addNode("f")->setRank(1);
    addEdge(graph, "a", "d", "ad", 1, 1);
    addEdge(graph, "a", "e", "ae", 1, 1);
    addEdge(graph, "a", "f", "af", 1, 1);
    addEdge(graph, "b", "d", "bd", 1, 1);
    addEdge(graph, "b", "e", "be", 1, 1);
    addEdge(graph, "b", "f", "bf", 1, 1);
    addEdge(graph, "c", "d", "cd", 1, 1);
    addEdge(graph, "c", "e", "ce", 1, 1);
    addEdge(graph, "c", "f", "cf", 1, 1);

    GraphUtils::setNodeIds(graph);
    GraphUtils::setNodeInAndOutEdges(graph);
    std::vector<std::vector<Node*>> layers = Order::initOrder(graph);
    int count = Order::twoLayerCrossing(layers[0], layers[1]);
    ASSERT_TRUE(count == 9);
}

TEST_F(OrderTest, should_count_cross_edges) {
    GraphLayout graph;
    graph.addNode("a")->setRank(0);
    graph.addNode("b")->setRank(0);
    graph.addNode("c")->setRank(0);
    graph.addNode("d")->setRank(1);
    graph.addNode("e")->setRank(1);
    graph.addNode("f")->setRank(1);
    graph.addNode("g")->setRank(2);
    graph.addNode("h")->setRank(2);
    addEdge(graph, "a", "d", "ad", 1, 1);
    addEdge(graph, "a", "e", "ae", 1, 1);
    addEdge(graph, "a", "f", "af", 1, 1);
    addEdge(graph, "b", "d", "bd", 1, 1);
    addEdge(graph, "b", "e", "be", 1, 1);
    addEdge(graph, "b", "f", "bf", 1, 1);
    addEdge(graph, "c", "d", "cd", 1, 1);
    addEdge(graph, "c", "e", "ce", 1, 1);
    addEdge(graph, "c", "f", "cf", 1, 1);
    addEdge(graph, "d", "g", "dg", 1, 1);
    addEdge(graph, "d", "h", "dh", 1, 1);
    addEdge(graph, "e", "g", "eg", 1, 1);

    GraphUtils::setNodeIds(graph);
    GraphUtils::setNodeInAndOutEdges(graph);
    std::vector<std::vector<Node*>> layers = Order::initOrder(graph);
    int count = Order::crossing(layers);
    ASSERT_TRUE(count == 10);
}

} // namespace testing
} // namespace arn