//
// Created by Łukasz Majcher on 03.09.2017.
//

#include <arachne/layouter/converter/svg_converter.hpp>
#include <gtest/gtest.h>

namespace arn {
namespace testing {
class SvgConverterTest : public ::testing::Test {};

TEST_F(SvgConverterTest, should_convert_graph_to_svg) {
    std::string expected =
            "<svg height=\"1000pt\" viewBox=\"0.00 0.00 1000 1000\" width=\"1000pt\" "
            "xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n  "
            " <g class=\"graph\" id=\"graph0\">\n      <g class=\"node\" id=\"a\">\n         "
            "<title>a</title>\n         <ellipse cx=\"0\" cy=\"0\" fill=\"none\" rx=\"0\" ry=\"0\" "
            "stroke=\"black\"></ellipse>\n         <text font-family=\"Times,serif\" "
            "font-size=\"14\" text-anchor=\"middle\" x=\"0\" y=\"0\">\n            <tspan "
            "dy=\"1.2em\" x=\"0\">a</tspan>\n         </text>\n      </g>\n      <g class=\"node\" "
            "id=\"b\">\n         <title>b</title>\n         <ellipse cx=\"0\" cy=\"0\" "
            "fill=\"none\" rx=\"0\" ry=\"0\" stroke=\"black\"></ellipse>\n         <text "
            "font-family=\"Times,serif\" font-size=\"14\" text-anchor=\"middle\" x=\"0\" "
            "y=\"0\">\n            <tspan dy=\"1.2em\" x=\"0\">b</tspan>\n         </text>\n      "
            "</g>\n      <g class=\"edge\" id=\"\">\n         <title></title>\n         <path "
            "d=\"M0 0 L0 0\" fill=\"none\" stroke=\"black\"></path>\n      </g>\n   </g>\n</svg>\n";

    GraphLayout graph;
    graph.setWidth(1000);
    graph.setHeight(1000);
    Node* a = graph.addNode("a");
    a->setLabel("a");
    a->setX(0);
    a->setY(0);
    a->setWidth(0);
    a->setHeight(0);
    Node* b = graph.addNode("b");
    b->setLabel("b");
    b->setX(0);
    b->setY(0);
    b->setWidth(0);
    b->setHeight(0);
    graph.addEdge("a", "b");

    SvgConverter converter;
    ASSERT_EQ(converter.createSvgElement(graph), expected);
}

} // namespace testing
} // namespace arn