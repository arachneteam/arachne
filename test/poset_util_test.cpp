#include <arachne/poset/gen/random_poset.hpp>
#include <arachne/poset/poset_builder.hpp>
#include <arachne/poset/poset_util.hpp>

#include <gtest/gtest.h>

namespace arn {
namespace testing {

class PosetUtilTest : public ::testing::Test {};

TEST(PosetUtilTest, total_order_decomposition) {
    // given
    Poset p = Poset::totalOrder(10);

    // when
    auto decomposition = minPairDecomposition(p);

    // then
    for (auto minPair : decomposition) {
        EXPECT_EQ(minPair.first, minPair.second);
    }
}

TEST(PosetUtilTest, two_disjoint_chains_decomposition) {
    // given
    Poset p = PosetBuilder(10)
            .addChain({0, 2, 4, 6, 8})
            .addChain({1, 3, 5, 7, 9})
            .build();

    // when
    auto decomposition = minPairDecomposition(p);

    // then
    EXPECT_EQ(p.size() / 2, decomposition.size());
    for (int i = 0; i < decomposition.size(); i++) {
        Poset::element_type a = decomposition[i].first;
        Poset::element_type b = decomposition[i].second;
        EXPECT_EQ(2 * i, std::min(a, b));
        EXPECT_EQ(2 * i + 1, std::max(a, b));
    }
}

TEST(PosetUtilTest, decomposition_invariants) {
    // given
    RandomPosetGenerator randomPosetGenerator;

    for (int repeat = 0; repeat < 100; repeat++) {
        // when
        Poset p = randomPosetGenerator.generate();
        auto decomposition = minPairDecomposition(p);

        // then
        std::vector<bool> visited(p.size(), false);
        for (auto minPair : decomposition) {
            Poset::element_type a = minPair.first;
            Poset::element_type b = minPair.second;
            EXPECT_TRUE(a == b || (p[a] || p[b]));
            visited[a] = visited[b] = true;
            for (int i = 0; i < visited.size(); i++) {
                if (!visited[i]) {
                    EXPECT_TRUE(p[a] < p[i] || (p[a] || p[i]));
                    EXPECT_TRUE(p[b] < p[i] || (p[b] || p[i]));
                }
            }
        }
    }
}

} // namespace testing
} // namespace arn
