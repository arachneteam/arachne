//
// Created by mtwarog on 1/25/18.
//
#include <arachne/graph/buffered_graph.hpp>
#include <arachne/graph/dfs.hpp>

#include <gmock/gmock-matchers.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace arn {
namespace testing {

using ::testing::ElementsAre;
using ::testing::UnorderedElementsAre;

class BufferedGraphTest : public ::testing::Test {};

TEST_F(BufferedGraphTest, basic_build_test) {
    using g_t = BufferedGraph<signed long long, signed short>;
    using e_t = g_t::edge_type;

    g_t G;

    EXPECT_TRUE((std::is_same<signed long long, g_t::vertex_index>::value));
    EXPECT_TRUE((std::is_same<signed short, g_t::edge_index>::value));

    G.addVertex(1);
    EXPECT_THAT(G.getVertices(), ElementsAre(1));

    G.addVertices({2, 3, 4});
    EXPECT_THAT(G.getVertices(), UnorderedElementsAre(1, 2, 3, 4));

    G.addEdge(1, 2);
    EXPECT_THAT(G.getEdges(), ElementsAre(e_t{0, 1, 2}));
}

TEST_F(BufferedGraphTest, remove_edge_test) {
    using g_t = BufferedGraph<>;
    using e_t = g_t::edge_type;

    g_t G;

    G.addVertices({1, 2, 3, 4, 5});

    G.addEdges({{1, 2}, {1, 3}, {1, 4}, {2, 4}, {3, 4}});

    EXPECT_THAT(G.getVertices(), UnorderedElementsAre(1, 2, 3, 4, 5));
    EXPECT_THAT(G.getEdges(), UnorderedElementsAre(e_t{0, 1, 2}, e_t{1, 1, 3}, e_t{2, 1, 4},
                                                   e_t{3, 2, 4}, e_t{4, 3, 4}));

    G.removeEdge(2);

    EXPECT_THAT(G.getVertices(), UnorderedElementsAre(1, 2, 3, 4, 5));
    EXPECT_THAT(G.getEdges(),
                UnorderedElementsAre(e_t{0, 1, 2}, e_t{1, 1, 3}, e_t{3, 2, 4}, e_t{4, 3, 4}));

    G.addEdge(1, 4);
    G.addEdge(4, 1);

    EXPECT_THAT(G.getVertices(), UnorderedElementsAre(1, 2, 3, 4, 5));
    EXPECT_THAT(G.getEdges(), UnorderedElementsAre(e_t{0, 1, 2}, e_t{1, 1, 3}, e_t{3, 2, 4},
                                                   e_t{4, 3, 4}, e_t{5, 1, 4}, e_t{6, 4, 1}));
}

TEST_F(BufferedGraphTest, remove_vertex_test) {
	using g_t = BufferedGraph<>;
	using e_t = g_t::edge_type;
	
	g_t G;
	
	G.addVertices({1, 2, 3, 4});
	G.addEdges({{1, 2}, {1, 3}, {1, 4}, {2, 1}, {3, 1}, {2, 4}});
	
	EXPECT_THAT(G.getVertices(), UnorderedElementsAre(1, 2, 3, 4));
	EXPECT_THAT(G.getEdges(), UnorderedElementsAre(e_t{0, 1, 2}, e_t{1, 1, 3}, e_t{2, 1, 4},
												   e_t{3, 2, 1}, e_t{4, 3, 1}, e_t{5, 2, 4}));
	
	G.removeVertex(1);
	
	EXPECT_THAT(G.getVertices(), UnorderedElementsAre(2, 3, 4));
	EXPECT_THAT(G.getEdges(), UnorderedElementsAre(e_t{5, 2, 4}));
	
	G.addVertex(1);
	G.addEdges({{1, 2}, {1, 3}});
	
	EXPECT_THAT(G.getVertices(), UnorderedElementsAre(1, 2, 3, 4));
	EXPECT_THAT(G.getEdges(), UnorderedElementsAre(e_t{5, 2, 4}, e_t{6, 1, 2}, e_t{7, 1, 3}));
	
	G.removeVertex(1);
	
	EXPECT_THAT(G.getVertices(), UnorderedElementsAre(2, 3, 4));
	EXPECT_THAT(G.getEdges(), UnorderedElementsAre(e_t{5, 2, 4}));
}

} // namespace testing
} // namespace arn
