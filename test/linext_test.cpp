#include <arachne/poset/gen/linext.hpp>
#include <arachne/poset/gen/random_poset.hpp>

#include <gtest/gtest.h>

#include <numeric>

namespace arn {
namespace testing {

class LinearExtensionGeneratorTest : public ::testing::Test {};

int factorial(int n) {
    int result = 1;
    for (int i = 2; i <= n; i++) {
        result *= i;
    }
    return result;
}

int uniqueHash(LinearExtension le) {
    int lexIdx = 0;
    std::vector<int> used(le.size(), 0);
    for (int i = 0; i < le.size(); i++) {
        int unused = le.size() - i - 1;
        int unusedLess = le[i] - std::accumulate(used.begin(), used.begin() + le[i], 0);
        lexIdx += unusedLess * factorial(unused);
        used[le[i]] = 1;
    }
    return lexIdx;
}

bool isCorrectExtension(LinearExtension le, const Poset& poset) {
    for (Poset::size_type i = 0; i < le.size(); i++) {
        for (Poset::size_type j = 0; j < i; j++) {
            if (poset[le[j]] > poset[le[i]]) {
                return false;
            }
        }
    }
    return true;
}

void assertAllLinearExtensionsGenerated(const Poset& poset, const std::set<int>& generatedExtensions) {
    std::vector<Poset::element_type> permutation(poset.size());
    std::iota(permutation.begin(), permutation.end(), 0);
    do {
        LinearExtension potentialExtension(permutation, std::vector<Poset::size_type>());
        if (isCorrectExtension(potentialExtension, poset)) {
            int hash = uniqueHash(potentialExtension);
            bool extensionGenerated = generatedExtensions.find(hash) != generatedExtensions.end();
            EXPECT_TRUE(extensionGenerated);
        }
    } while (std::next_permutation(permutation.begin(), permutation.end()));
}

TEST(LinearExtensionGeneratorTest, total_order) {
    // given
    Poset p = Poset::totalOrder(10);

    int counter = 0;

    // when
    auto stats = LinearExtensionGenerator()
            .forEachExtension([&](LinearExtension le) {
                ++counter;
                EXPECT_EQ(0, uniqueHash(le));
            })
            .generate(p);

    // then
    EXPECT_EQ(1, counter);
    EXPECT_EQ(counter, stats.countTotal());
    for (Poset::element_type x = 0; x < p.size(); x++) {
        EXPECT_DOUBLE_EQ(x + 1, stats.averageHeight(x));
        for (Poset::element_type y = x + 1; y < p.size(); y++) {
            EXPECT_EQ(counter, stats.countWithOrder(x, y));
            EXPECT_DOUBLE_EQ(1.0, stats.probabilityOfOrder(x, y));
            EXPECT_EQ(0, stats.countWithOrder(y, x));
            EXPECT_DOUBLE_EQ(0.0, stats.probabilityOfOrder(y, x));
        }
    }
}

TEST(LinearExtensionGeneratorTest, total_unorder) {
    // given
    Poset p = Poset::totalUnorder(8);

    int counter = 0;
    std::set<int> hashedExtensions;

    // when
    auto stats = LinearExtensionGenerator()
            .forEachExtension([&](LinearExtension le) {
                ++counter;
                hashedExtensions.insert(uniqueHash(le));
            })
            .generate(p);

    // then
    EXPECT_EQ(factorial(p.size()), counter);
    EXPECT_EQ(counter, stats.countTotal());
    EXPECT_EQ(counter, hashedExtensions.size());
    double expectedHeight = (p.size() + 1) / 2.0;
    for (Poset::element_type x = 0; x < p.size(); x++) {
        EXPECT_DOUBLE_EQ(expectedHeight, stats.averageHeight(x));
        for (Poset::element_type y = x + 1; y < p.size(); y++) {
            EXPECT_EQ(counter / 2, stats.countWithOrder(x, y));
            EXPECT_EQ(counter / 2, stats.countWithOrder(y, x));
            EXPECT_DOUBLE_EQ(0.5, stats.probabilityOfOrder(x, y));
            EXPECT_DOUBLE_EQ(0.5, stats.probabilityOfOrder(y, x));
        }
    }
}

TEST(LinearExtensionGeneratorTest, random_poset) {
    RandomPosetGenerator randomPosetGenerator;

    for (int repeat = 0; repeat < 100; repeat++) {
        // given
        Poset::size_type posetSize = static_cast<Poset::size_type>(5 + rand() % 5);
        Poset p = randomPosetGenerator.generate(posetSize);
        std::set<int> hashedExtensions;
        Poset::element_type a = rand() % p.size();
        Poset::element_type b = rand() % p.size();
        int orderSumAB = 0;
        int heightSumA = 0;

        // when
        auto stats = LinearExtensionGenerator()
                .forEachExtension([&] (LinearExtension le) {
                    // then
                    bool firstOccurrence = hashedExtensions.insert(uniqueHash(le)).second;
                    EXPECT_TRUE(firstOccurrence);
                    EXPECT_TRUE(isCorrectExtension(le, p));
                    orderSumAB += le.idx(a) < le.idx(b) ? 1 : 0;
                    heightSumA += le.idx(a) + 1;
                })
                .generate(p);

        // then
        assertAllLinearExtensionsGenerated(p, hashedExtensions);
        EXPECT_EQ(hashedExtensions.size(), stats.countTotal());
        EXPECT_EQ(orderSumAB, stats.countWithOrder(a, b));
        EXPECT_DOUBLE_EQ(static_cast<double>(orderSumAB) / stats.countTotal(), stats.probabilityOfOrder(a, b));
        EXPECT_DOUBLE_EQ(static_cast<double>(heightSumA) / stats.countTotal(), stats.averageHeight(a));
        for (Poset::element_type x = 0; x < p.size(); x++) {
            for (Poset::element_type y = x + 1; y < p.size(); y++) {
                EXPECT_EQ(hashedExtensions.size(), stats.countWithOrder(x, y) + stats.countWithOrder(y, x));
                EXPECT_DOUBLE_EQ(1.0, stats.probabilityOfOrder(x, y) + stats.probabilityOfOrder(y, x));
            }
        }
    }
}

} // namespace testing
} // namespace arn
