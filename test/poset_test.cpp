#include <arachne/poset/gen/random_poset.hpp>
#include <arachne/poset/poset_builder.hpp>

#include <gtest/gtest.h>

namespace arn {
namespace testing {

class PosetTest : public ::testing::Test {};

TEST(PosetTest, simple_poset) {
    // given
    Poset p = PosetBuilder(7)
            .add(4, 6).add(5, 6)
            .add(3, 4).add(3, 5)
            .add(2, 3)
            .add(0, 2).add(1, 2)
            .build();

    // then
    EXPECT_TRUE(p[0] < p[6]);
    EXPECT_FALSE(p[5] < p[2]);
    EXPECT_FALSE(p[3] < p[3]);
    EXPECT_TRUE(p[3] <= p[3]);
    EXPECT_TRUE(p[6] > p[1]);
    EXPECT_TRUE(p[6] >= p[0]);
    EXPECT_TRUE(p[0] || p[1]);
}

TEST(PosetTest, operator_invariants) {
    // given
    RandomPosetGenerator randomPosetGenerator;

    for (int repeat = 0; repeat < 100; repeat++) {
        // when
        Poset p = randomPosetGenerator.generate();

        // then
        for (Poset::element_type i = 0; i < p.size(); i++) {
            for (Poset::element_type j = 0; j < p.size(); j++) {
                EXPECT_TRUE(p[i] <= p[j] || p[i] >= p[j] || (p[i] || p[j]));
                EXPECT_FALSE(p[i] < p[j] && p[i] >= p[j]);
                EXPECT_FALSE(p[i] > p[j] && p[i] <= p[j]);
                EXPECT_FALSE((p[i] || p[j]) && (p[i] <= p[j] || p[i] >= p[j]));
                EXPECT_FALSE(p[i] <= p[j] && p[i] >= p[j] && i != j);
                EXPECT_FALSE(p[i] < p[j] && !(p[i] <= p[j]));
                EXPECT_FALSE(p[i] > p[j] && !(p[i] >= p[j]));
            }
        }
    }
}

TEST(PosetTest, total_order) {
    // given
    Poset p = Poset::totalOrder(10);

    // then
    for (Poset::element_type i = 0; i < p.size(); i++) {
        for (Poset::element_type j = i + 1; j < p.size(); j++) {
            EXPECT_TRUE(p[i] < p[j]);
            EXPECT_TRUE(p[i] <= p[j]);
            EXPECT_FALSE(p[i] > p[j]);
            EXPECT_FALSE(p[i] >= p[j]);
            EXPECT_FALSE(p[i] || p[j]);
        }
    }
}

TEST(PosetTest, total_unorder) {
    // given
    Poset p = Poset::totalUnorder(10);

    // then
    for (Poset::element_type i = 0; i < p.size(); i++) {
        for (Poset::element_type j = i + 1; j < p.size(); j++) {
            EXPECT_TRUE(p[i] || p[j]);
            EXPECT_FALSE(p[i] < p[j]);
            EXPECT_FALSE(p[i] <= p[j]);
            EXPECT_FALSE(p[i] > p[j]);
            EXPECT_FALSE(p[i] >= p[j]);
        }
    }
}

TEST(PosetTest, two_disjoint_chains) {
    // given
    Poset p = PosetBuilder(10)
            .addChain({0, 2, 4, 6, 8})
            .addChain({1, 3, 5, 7, 9})
            .build();

    // then
    for (Poset::element_type i = 0; i < p.size(); i++) {
        for (Poset::element_type j = i + 1; j < p.size(); j++) {
            if (i % 2 == j % 2) {
                EXPECT_TRUE(p[i] < p[j]);
            } else {
                EXPECT_TRUE(p[i] || p[j]);
            }
        }
    }
}

TEST(PosetTest, two_interleaved_chains) {
    // given
    Poset p = PosetBuilder(10)
            .addChain({0, 2, 4, 6, 8})
            .addChain({1, 3, 5, 7, 9})
            .add(1, 2).add(3, 4).add(5, 6).add(7, 8)
            .build();

    // then
    for (Poset::element_type i = 0; i < p.size(); i++) {
        for (Poset::element_type j = i + 1; j < p.size(); j++) {
            if (i % 2 == 1 || j % 2 == 0) {
                EXPECT_TRUE(p[i] < p[j]);
            } else {
                EXPECT_TRUE(p[i] || p[j]);
            }
        }
    }
}

} // namespace testing
} // namespace arn