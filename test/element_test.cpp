//
// Created by Łukasz Majcher on 28.08.2017.
//

#include <arachne/layouter/converter/element.hpp>
#include <gtest/gtest.h>

namespace arn {
namespace testing {
class ElementTest : public ::testing::Test {};

TEST_F(ElementTest, should_create_simple_element) {
    arn::Element element("g");
        std::cout<<element.print();
    ASSERT_EQ(element.print(), "<g></g>\n");

    element.addAttribute("id", "node1");
    ASSERT_EQ(element.print(), "<g id=\"node1\"></g>\n");

    arn::Element child("ellipse");
    element.addChild(&child);
    ASSERT_EQ(element.print(), "<g id=\"node1\">\n   <ellipse></ellipse>\n</g>\n");

    child.addTextValue("value");
    ASSERT_EQ(element.print(), "<g id=\"node1\">\n   <ellipse>value</ellipse>\n</g>\n");

    child.addAttribute("x", "10");
    child.addAttribute("y", "1");
    ASSERT_EQ(element.print(), "<g id=\"node1\">\n   <ellipse x=\"10\" y=\"1\">value</ellipse>\n</g>\n");

    arn::Element child2("text");
    element.addChild(&child2);
    ASSERT_EQ(element.print(),
              "<g id=\"node1\">\n   <ellipse x=\"10\" y=\"1\">value</ellipse>\n   <text></text>\n</g>\n");
}
} // namespace testing
} // namespace arn
