
//
// Created by Łukasz Majcher on 06.09.2017.
//

#include "arachne/layouter/rank/cycle_remover.hpp"
#include <arachne/layouter/layouters/layouter.hpp>
#include <arachne/layouter/model/tree.hpp>
#include <arachne/layouter/parser/parser.h>
#include <arachne/layouter/rank/feasible_tree.hpp>
#include <arachne/layouter/utils/graph_utils.hpp>
#include <arachne/layouter/utils/tree_utils.hpp>
#include <gtest/gtest.h>

namespace arn {
namespace testing {
class TreeUtilsTest : public ::testing::Test {};

TEST_F(TreeUtilsTest, should_postorder_dag) {
    GraphLayout graph;
    graph.addNode("a");
    graph.addNode("b");
    graph.addNode("c");
    Edge* edge1 = graph.addEdge("a", "b");
    edge1->setLabel("ab");
    edge1->setMinLen(1);
    edge1->setWeight(1);

    Edge* edge2 = graph.addEdge("b", "c");
    edge2->setLabel("bc");
    edge2->setMinLen(1);
    edge2->setWeight(1);

    Edge* edge3 = graph.addEdge("a", "c");
    edge3->setLabel("ac");
    edge3->setMinLen(3);
    edge3->setWeight(1);

    graph = GraphUtils::setNodeIds(graph);
    graph = GraphUtils::setNodeInAndOutEdges(graph);

    Tree tree = FeasibleTree::findFeasibleTree(graph);
    TreeUtils::assignPostorder(tree.getRoot(), graph.size());

    std::vector<int> expectedLims = {2, 0, 1};
    std::vector<int> actualLims;
    for (auto node : tree.getNodes()) {
        actualLims.push_back(node->getLim());
    }
    ASSERT_EQ(actualLims.size(), expectedLims.size());
    for (auto i = 0; i < expectedLims.size(); i++) {
        ASSERT_EQ(actualLims[i], expectedLims[i]);
    }

    std::vector<int> expectedLows = {0, 0, 0};
    std::vector<int> actualLows;
    for (auto node : tree.getNodes()) {
        actualLows.push_back(node->getLow());
    }
    ASSERT_EQ(actualLows.size(), expectedLows.size());
    for (auto i = 0; i < expectedLows.size(); i++) {
        ASSERT_EQ(actualLows[i], expectedLows[i]);
    }
}

TEST_F(TreeUtilsTest, should_compute_cut_values) {
    Parser parser;
    GraphLayout graph = parser.parse("rank.txt");

    graph = GraphUtils::setNodeIds(graph);
    graph = GraphUtils::makeSimpleGraph(graph);
    graph = GraphUtils::setNodeInAndOutEdges(graph);
    graph = CycleRemover::execute(graph);
    graph = GraphUtils::assignRank(graph);

    Tree tree = FeasibleTree::findFeasibleTree(graph);
    TreeUtils::assignPostorder(tree.getRoot(), graph.size());
    TreeUtils::assignCutValues(tree, graph);

    std::vector<int> expectedCuts = {3, 3, 0, 0, 3, 3, -1};
    std::vector<int> actualCuts;
    for (auto edge : tree.getEdges()) {
        actualCuts.push_back(tree.getEdgeCutValue(edge.first, edge.second));
    }
    ASSERT_EQ(actualCuts.size(), expectedCuts.size());
    for (auto i = 0; i < expectedCuts.size(); i++) {
        ASSERT_EQ(actualCuts[i], expectedCuts[i]);
    }
}

TEST_F(TreeUtilsTest, should_remove_empty_ranks) {
    GraphLayout graph;
    graph.addNode("a")->setRank(-3);
    graph.addNode("b")->setRank(0);
    graph.addNode("c")->setRank(2);
    graph.addNode("d")->setRank(-5);

    TreeUtils::removeEmptyRanks(graph);

    std::vector<int> expectedRanks = {1, 2, 3, 0};
    std::vector<int> actualRanks;
    for (auto node : graph.getNodes()) {
        actualRanks.push_back(node->getRank());
    }
    ASSERT_EQ(actualRanks.size(), expectedRanks.size());
    for (auto i = 0; i < expectedRanks.size(); i++) {
        ASSERT_EQ(actualRanks[i], expectedRanks[i]);
    }
}

TEST_F(TreeUtilsTest, should_normalize_ranks) {
    GraphLayout graph;
    graph.addNode("a")->setRank(-3);
    graph.addNode("b")->setRank(0);
    graph.addNode("c")->setRank(2);
    graph.addNode("d")->setRank(-5);

    TreeUtils::normalizeRanks(graph);

    std::vector<int> expectedRanks = {2, 5, 7, 0};
    std::vector<int> actualRanks;
    for (auto node : graph.getNodes()) {
        actualRanks.push_back(node->getRank());
    }
    ASSERT_EQ(actualRanks.size(), expectedRanks.size());
    for (auto i = 0; i < expectedRanks.size(); i++) {
        ASSERT_EQ(actualRanks[i], expectedRanks[i]);
    }
}

} // namespace testing
} // namespace arn