//
// Created by Łukasz Majcher on 04.09.2017.
//

#include <arachne/layouter/layouters/layouter.hpp>
#include <arachne/layouter/utils/graph_utils.hpp>
#include <gtest/gtest.h>
#include <vector>

namespace arn {
namespace testing {
class GraphUtilsTest : public ::testing::Test {};

TEST_F(GraphUtilsTest, should_set_node_ids) {
    GraphLayout graph;
    graph.addNode("a");
    graph.addNode("b");
    graph.addNode("c");

    graph = GraphUtils::setNodeIds(graph);
    for (int i = 0; i < graph.getNodes().size(); i++) {
        ASSERT_EQ(graph.getNodes()[i]->getId(), i);
    }
}

TEST_F(GraphUtilsTest, should_merge_multiedges) {
    GraphLayout graph;
    graph.addNode("a");
    graph.addNode("b");
    graph.addNode("c");
    Edge* edge1 = graph.addEdge("a", "b");
    edge1->setLabel("ab1");
    edge1->setMinLen(1);
    edge1->setWeight(1);

    Edge* edge2 = graph.addEdge("a", "b");
    edge2->setLabel("ab2");
    edge2->setMinLen(2);
    edge2->setWeight(1);

    Edge* edge3 = graph.addEdge("a", "b");
    edge3->setLabel("ab3");
    edge3->setMinLen(3);
    edge3->setWeight(1);

    graph = GraphUtils::makeSimpleGraph(graph);

    ASSERT_TRUE(graph.getEdges().size() == 1);
    Edge* edge = graph.getEdges()[0];
    ASSERT_EQ(edge->getId(), 0);
    ASSERT_EQ(edge->getLabel(), "ab1|ab2|ab3");
    ASSERT_EQ(edge->getMinLen(), 3);
    ASSERT_EQ(edge->getWeight(), 3);
}

TEST_F(GraphUtilsTest, should_rank_dag) {
    GraphLayout graph;
    graph.addNode("a");
    graph.addNode("b");
    graph.addNode("c");
    Edge* edge1 = graph.addEdge("a", "b");
    edge1->setLabel("ab");
    edge1->setMinLen(1);
    edge1->setWeight(1);

    Edge* edge2 = graph.addEdge("b", "c");
    edge2->setLabel("bc");
    edge2->setMinLen(1);
    edge2->setWeight(1);

    Edge* edge3 = graph.addEdge("a", "c");
    edge3->setLabel("ac");
    edge3->setMinLen(3);
    edge3->setWeight(1);

    graph = GraphUtils::setNodeIds(graph);
    graph = GraphUtils::setNodeInAndOutEdges(graph);
    graph = GraphUtils::assignRank(graph);

    std::vector<int> expectedRanks = {-3, -1, 0};
    std::vector<int> actualRanks;
    for (auto node : graph.getNodes()) {
        actualRanks.push_back(node->getRank());
    }
    ASSERT_EQ(actualRanks.size(), expectedRanks.size());
    for (auto i = 0; i < expectedRanks.size(); i++) {
        ASSERT_EQ(actualRanks[i], expectedRanks[i]);
    }
}

TEST_F(GraphUtilsTest, should_normalize_edges) {
    GraphLayout graph;
    graph.addNode("a")->setRank(0);
    graph.addNode("b")->setRank(2);
    graph.addNode("c")->setRank(3);
    Edge* edge1 = graph.addEdge("a", "b");
    edge1->setLabel("ab");
    edge1->setMinLen(1);
    edge1->setWeight(1);

    Edge* edge2 = graph.addEdge("b", "c");
    edge2->setLabel("bc");
    edge2->setMinLen(1);
    edge2->setWeight(1);

    Edge* edge3 = graph.addEdge("a", "c");
    edge3->setLabel("ac");
    edge3->setMinLen(3);
    edge3->setWeight(1);

    graph = GraphUtils::normalizeEdges(graph);

    std::vector<std::string> expectedNodes = {"a", "b", "c", "dummy0", "dummy1", "dummy2"};
    std::vector<std::string> actualNodes;
    for (auto node : graph.getNodes()) {
        actualNodes.push_back(node->getLabel());
    }
    ASSERT_EQ(actualNodes.size(), expectedNodes.size());
    for (auto i = 0; i < expectedNodes.size(); i++) {
        ASSERT_EQ(actualNodes[i], expectedNodes[i]);
    }

    std::vector<std::string> expectedEdges = {"dummy0->b", "b->c",           "a->dummy0",
                                              "a->dummy1", "dummy1->dummy2", "dummy2->c"};
    std::vector<std::string> actualEdges;
    for (auto edge : graph.getEdges()) {
        actualEdges.push_back(edge->getHead()->getLabel() + "->" + edge->getTail()->getLabel());
    }
    ASSERT_EQ(actualEdges.size(), expectedEdges.size());
    for (auto i = 0; i < expectedEdges.size(); i++) {
        ASSERT_EQ(actualEdges[i], expectedEdges[i]);
    }
}

TEST_F(GraphUtilsTest, should_create_layers) {
    GraphLayout graph;
    graph.addNode("a")->setRank(0);
    graph.addNode("b")->setRank(2);
    graph.addNode("c")->setRank(3);
    Edge* edge1 = graph.addEdge("a", "b");
    edge1->setLabel("ab");
    edge1->setMinLen(1);
    edge1->setWeight(1);

    Edge* edge2 = graph.addEdge("b", "c");
    edge2->setLabel("bc");
    edge2->setMinLen(1);
    edge2->setWeight(1);

    Edge* edge3 = graph.addEdge("a", "c");
    edge3->setLabel("ac");
    edge3->setMinLen(3);
    edge3->setWeight(1);

    std::vector<std::vector<Node*>> layers = GraphUtils::toLayers(graph);

    ASSERT_EQ(layers[0].back()->getLabel(), "a");
    ASSERT_EQ(layers[2].back()->getLabel(), "b");
    ASSERT_EQ(layers[3].back()->getLabel(), "c");
    ASSERT_TRUE(layers[1].empty());
}

} // namespace testing
} // namespace arn