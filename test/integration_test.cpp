//
// Created by Łukasz Majcher on 29.08.2017.
//

#include <arachne/layouter/converter/svg_converter.hpp>
#include <arachne/layouter/layouters/dummy_layouter.hpp>
#include <arachne/layouter/layouters/hierarchical_layouter.hpp>
#include <arachne/layouter/parser/parser.h>
#include <gtest/gtest.h>

namespace arn {
namespace testing {
class IntegrationTest : public ::testing::Test {};

TEST_F(IntegrationTest, should_convert_graph_to_svg) {
    Parser parser;

    GraphLayout graph = parser.parse("test_eight.txt");

    Layouter* layouter = new DummyLayouter();
    graph = layouter->execute(graph);

    SvgConverter converter;
    converter.convert(graph, "output.svg");
}

TEST_F(IntegrationTest, should_convert_hierarchical_graph_to_svg) {
    Parser parser;

    GraphLayout graph = parser.parse("graph.dot");

    Layouter* layouter = new HierarchicalLayouter();
    graph = layouter->execute(graph);

    SvgConverter converter;
    converter.convert(graph, "output.svg");
}

TEST_F(IntegrationTest, should_rank_graph) {
    Parser parser;

    GraphLayout graph = parser.parse("rank.txt");

    Layouter* layouter = new HierarchicalLayouter();
    graph = layouter->execute(graph);

    std::vector<int> expectedRanks = {0, 1, 1, 1, 2, 3, 2, 4, 3};
    std::vector<int> actualRanks;
    for (auto node : graph.getNodes()) {
        actualRanks.push_back(node->getRank());
    }
    ASSERT_EQ(actualRanks.size(), expectedRanks.size());
    for (auto i = 0; i < expectedRanks.size(); i++) {
        ASSERT_EQ(actualRanks[i], expectedRanks[i]);
    }
}

} // namespace testing
} // namespace arn