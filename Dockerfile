#Base system image
FROM ubuntu:18.04

#Install necessary libraries
RUN apt-get update
RUN apt-get install git build-essential cmake libboost-coroutine1.65-dev libboost-context1.65-dev libgtest-dev -y

#Build gtest sources
RUN mkdir gtest-build
WORKDIR /gtest-build
RUN cmake -DCMAKE_BUILD_TYPE=RELEASE /usr/src/gtest/
RUN make
RUN mv libg* /usr/lib/

