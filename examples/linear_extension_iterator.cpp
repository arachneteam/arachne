#include <arachne/poset/gen/linext.hpp>

void printExtension(arn::LinearExtension le) {
    for (arn::Poset::size_type i = 0; i < le.size(); i++) {
        if (i > 0) {
            printf(",");
        }
        printf("%u", le[i]);
    }
    printf("\n");
}

int main() {
    arn::Poset p = arn::Poset::totalUnorder(40);

    int extensionCounterLimit = 10;
    int extensionCounter = 0;

    for (arn::LinearExtension le : arn::linearExtensions(p)) {
        printExtension(le);
        if (++extensionCounter >= extensionCounterLimit) {
            break;
        }
    }

    return 0;
}
