//
// Created by mtwarog on 1/15/18.
//

#include <arachne/graph/compact_graph.hpp>
#include <arachne/graph/dfs.hpp>
#include <iostream>

int main() {

    // CompactGraph sized for 0 vertices
    arn::CompactGraph<> G;

    std::cout << "G size = " << G.getSize() << std::endl;

    // custom vertex_index type
    std::cout << "CompactGraph<signed int>::vertex_index = unsigned int? "
              << std::boolalpha << std::is_same<arn::CompactGraph<signed int>::vertex_index, signed int>::value << std::endl;

    // custom edge_index type
    std::cout << "CompactGraph<int, char>::edge_index = char? "
              << std::boolalpha << std::is_same<arn::CompactGraph<int, char>::edge_index, char>::value << std::endl;

    // resize for 5 vertices
    G.setSize(5);

    std::cout << "Vertices in G: ";

    // auto may be more readable
    for (arn::CompactGraph<>::vertex_index v : G.getVertices())
        std::cout << v << " ";

    std::cout << std::endl;

    // getVertices return range
    std::cout << "Also vertices in G: ";
    for (arn::CompactGraph<>::vertex_iterator it = G.getVertices().begin(),
                                              end = G.getVertices().end();
         it != end; ++it)
        std::cout << *it << " ";
    std::cout << std::endl;

    // adding edge
    G.addEdge(0, 3);
    G.addEdge(3, 4);

    std::cout << "G edges:" << std::endl;
    for (arn::CompactGraph<>::edge_type e : G.getEdges())
        std::cout << "  " << e << std::endl;

    G.addEdge(0, 1);
    G.addEdge(1, 2);

    // for CompactGraph order may not be the same as their creation order
    std::cout << "Also G edges:" << std::endl;
    for (arn::CompactGraph<>::edge_iterator it = G.getEdges().begin(), end = G.getEdges().end();
         it != end; ++it)
        std::cout << "  " << it->idx << ": " << it->begin << " " << it->end << std::endl;
	
	//DFS order of verticles starting with 0
	std::cout<<"DFS order: ";
	for(auto v : arn::DFSOrder(G, 0))
		std::cout<<v<<" ";
	
    return 0;
}
