#include <arachne/poset/poset_util.hpp>
#include <arachne/poset/poset_builder.hpp>

#include <cstdio>

int main() {
    arn::Poset p = arn::PosetBuilder(5)
            .add(2, 4).add(3, 4)
            .add(0, 2).add(1, 2).add(1, 3)
            .build();

    auto decomposition = arn::minPairDecomposition(p);
    printf("Min pair decomposition:\n");
    for (auto minPair : decomposition) {
        printf("%u %u\n", minPair.first, minPair.second);
    }

    return 0;
}
