#include <arachne/poset/gen/linext.hpp>
#include <arachne/poset/poset_builder.hpp>

void printExtension(arn::LinearExtension le) {
    for (arn::Poset::size_type i = 0; i < le.size(); i++) {
        if (i > 0) {
            printf(",");
        }
        printf("%u", le[i]);
    }
    printf("\n");
}

int main() {
    arn::Poset p = arn::PosetBuilder(6)
            .add(1, 4).add(1, 5).add(3, 4).add(3, 5)
            .add(0, 1).add(2, 3)
            .build();

    arn::LinearExtensionGenerator linearExtensionGenerator;
    auto stats = linearExtensionGenerator
            .forEachExtension([](arn::LinearExtension le) {
                printExtension(le);
            })
            .generate(p);

    printf("Number of linear extensions = %d\n", stats.countTotal());
    printf("Number of linear extensions with 0 < 3 = %d\n", stats.countWithOrder(0, 3));
    printf("Probability of 0 < 3 in random extension = %.2lf\n", stats.probabilityOfOrder(0, 3));
    printf("Average height of 3 = %.2lf\n", stats.averageHeight(3));

    return 0;
}
