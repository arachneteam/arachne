//
// Created by Łukasz Majcher on 19.08.2017.
//

#include <arachne/layouter/parser/parser.h>
#include <fstream>
#include <iostream>

using namespace std;

int main() {
    arn::Parser parser;
    parser.parse("dane.txt");
}
