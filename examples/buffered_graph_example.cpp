//
// Created by mtwarog on 1/16/18.
//

#include <arachne/graph/buffered_graph.hpp>
#include <iostream>
#include <type_traits>
#include <vector>
#include <set>

using std::cout;
using std::endl;

int main() {

    // empty graph with default index types for vertices and edges
    arn::BufferedGraph<> G;

    G.addVertex(4);
    G.addVertex(9);

    // vertices are not ordered
    cout << "Vertices in G: ";
    for (auto v : G.getVertices())
        cout << v << " ";
    cout << endl;

    // can use range to add vertices
    G.addVertices(boost::integer_range<int>(6, 8));

    cout << "Vertices in G: ";
    for (auto v : G.getVertices())
        cout << v << " ";
    cout << endl;

    // types convertible to G::verticle_type can be used so int works
    std::vector<long long> v = {1, 11, 21};

    G.addVertices(v.begin(), v.end());

    // initializer list works too
    G.addVertices({2, 3, 5});

    cout << "Vertices in G: ";
    for (auto v : G.getVertices())
        cout << v << " ";
    cout << endl;

	//add single edge
    G.addEdge(1, 11);
	
    using v_type = arn::BufferedGraph<>::vertex_index;
    std::vector<std::pair<v_type, v_type>> vec = {{6, 1}, {6, 7}, {6, 9}};
    
	//any class defining begin and end works
    G.addEdges(vec);
	
	std::set<std::pair<v_type, v_type>> s = {{9, 21}, {5, 6}, {2, 11}};
	
	G.addEdges(s);
	
	//initializer list is ok as well
	G.addEdges({{3, 11}, {5, 11}, {11, 1}, {11, 6}});

    cout << "Edges in G:" << endl;
    for (auto e : G.getEdges())
        cout << "  " << e << endl;

    cout << "Edge number 2:  " << G.getEdge(2) << endl;

	//remove edge by id
    G.removeEdge(2);

    cout << "Edges in G:" << endl;
    for (auto e : G.getEdges())
        cout << "  " << e << endl;

    cout << "Edges incoming into vertex 11:" << endl;
    for (auto e : G.incoming(11))
        cout << "  " << e << endl;

    //remove vertex
    G.removeVertex(11);

    cout << "Vertices in G after removal: ";
    for (auto v : G.getVertices())
        cout << v << " ";
    cout << endl;

    cout << "Edges in G after removal:" << endl;
    for (auto e : G.getEdges())
        cout << "  " << e << endl;

    return 0;
}
