//
// Created by mtwarog on 1/17/18.
//

#include <arachne/graph/buffered_graph.hpp>
#include <arachne/graph/dfs.hpp>
#include <iostream>
#include <vector>

using std::cout;
using std::endl;

struct PreVisitor {
  	std::set<int> set;
  	
  	bool visited(int v) {
		return set.find(v) == set.end();
	}
  
  	void preorder(int v) {
		set.insert(v);
		cout<<v<<" ";
	}
};

struct PostVisitor {
  std::set<int> set;
  
  bool visited(int v) {
	  bool ret = set.find(v) == set.end();
	  set.insert(v);
	  return ret;
  }
  
  void postorder(int v) {
	  cout<<v<<" ";
  }
};

struct PrePostVisitor {
  std::set<int> set;
  std::vector<int> preorder_v;
  std::vector<int> postorder_v;
  
  bool visited(int v) {
	  return set.find(v) == set.end();
  }
  
  void preorder(int v) {
	  set.insert(v);
	  preorder_v.push_back(v);
  }
  
  void postorder(int v) {
	  postorder_v.push_back(v);
  }
};

int main() {

    arn::BufferedGraph<> G;
    G.addVertices({1, 3, 5, 7});
	
	G.addEdge(1, 3);
	G.addEdge(1, 5);
	G.addEdge(5, 1);
	G.addEdge(5, 7);
	
	cout<<"Vertices in G: ";
	for(auto v : arn::DFSOrder(G, 1))
		cout<<v<<" ";
	cout<<endl;
	
	cout<<"Sample preorder: ";
	arn::DFS(G, PreVisitor(), 1);
	cout<<endl;
	
	cout<<"Sample postorder: ";
	arn::DFS(G, PostVisitor(), 1);
	cout<<endl;
	
	PrePostVisitor vi;
	arn::DFS(G, vi, 1);
	
	cout<<"Preorder again: ";
	for(auto v : vi.preorder_v)
		cout<<v<<" ";
	cout<<endl;
	
	cout<<"Postorder again: ";
	for(auto v : vi.postorder_v)
		cout<<v<<" ";
	cout<<endl;
	
    return 0;
}